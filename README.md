# Web API Communication Demo

## About

### Author

Ryan E. Anderson

---

### Description

This solution demonstrates how an end user can communicate with a third-party web API through an intermediary web API.

- Security features are optional.
- Choose how many times the third-party API should process information provided by an end user.
- Set an option for determining how long to wait for information to be processed.
- Target different servers that are able to communicate with the intermediary web API through a callback route.
- Control the delay between background tasks that process information provided by an end user.

---

### Version

0.1.0

---

### License

Apache-2.0

---

## Solution

This solution contains two web API projects and five class libraries. Services, clients, providers, options, and a 
manager were created to promote clean, cohesive code. The project for the intermediary has two web API controllers and a 
total of three endpoints. The project for the third-party web API has one controller that contains a single endpoint.

---

## Intermediary Communication API

This web API allows communication between an end user and a third-party web API.

### Base Path

#### /intermediary/v0.1/communication

There are two endpoints at this path.

#### /request

##### HTTP POST

This method can be used to send information to a third-party web API for processing.

Below is a sample payload that can be sent to this endpoint.

```JSON
{
    "body": "TEST"
}
```

Below is a sample response.

```text
3a1fc882-9c2f-40e9-bbad-03ec00799e3f
```

### Endpoints

#### /callback

This endpoint can be used by a third-party web API to send status updates. 

##### HTTP POST

This method can be used to send a request to a third-party web API so that processing of information can begin.

Below is a sample payload.

```JSON
{
    "body": "TEST"
}
```

A status code of 204 will be returned from this endpoint.

##### HTTP PUT

This method can be used to accept a callback request from a third-party web API to make status updates.

Below is a sample payload.

```JSON
{
    "status": "COMPLETED",
    "detail": "The process has completed."
}
```

A status code of 204 will be returned from this endpoint.

## Intermediary Information API

This web API can be used to retrieve a record that contains a status and other information about work that is being 
performed by a third-party web API.

### Base Path

#### /intermediary/v0.1/information

There is one endpoint at this path.

### Endpoints

#### /status/{id}

##### HTTP GET

This method can be used to retrieve records that contain information about the processing of information.

Below is a sample request route.

```text
/intermediary/v0.1/information/status/df397e72-c32a-474a-88a7-b2e01dd3c74e
```

Below is a sample response.

```json
{
    "status": "COMPLETED",
    "detail": "The record identified by TEST was updated.",
    "body": "TEST",
    "creationTimeStamp": "2020-01-12T05:04:19.1620734Z",
    "lastUpdatedTimeStamp": "2020-01-12T05:05:10.7002621Z",
    "id": "3a1fc882-9c2f-40e9-bbad-03ec00799e3f"
}
```

## Third Party Work API

This web API can be used to process information provided by the Intermediary Communication API.

### Base Path

#### /thirdparty/v0.1/work

There is one endpoint at this path.

### Endpoints

#### /process

##### HTTP POST

This method can be used to relay different statuses to the Intermediary Communication Web API while provided information 
is being processed.

Below is a sample payload.

```JSON
{
    "body": "TEST",
    "callback": "/callback"
}
```

A status code of 204 will be returned from this endpoint.

---

## Security 

The "Basic" HTTP Authentication scheme was implemented to secure the callback routes. It can be controlled through an 
attribute.

---

## Options

Options can be used to control the behavior of each API. Options can be set in the appsettings.json file of each 
application. The Startup.cs file can also be edited to set options.

---

## Using the Web API Communication Demo

1. Find the appsettings.json file at the root of the WebApiCommunicationDemo.Api.Web.Intermediary project, and modify 
the URI information so that an appropriate server for a third-party web API will be used as a target.
2. Repeat the first step for WebApiCommunicationDemo.Api.Web.ThirdParty.
3. Build the solution to ensure that no issues are encountered.
4. Deploy both of the applications of this demo to a server.
5. Run both applications.
6. Send an HTTP POST request to /intermediary/v0.1/communication/request to start processing information (A sample 
payload was provided in a previous section.). The response should return a GUID if the request succeeds.
7. Use the GUID from the previous step to check the status of a record.