﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Model Validation Attribute
 * 
 * by Ryan E. Anderson
 *  
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace WebApiCommunicationDemo.Api.Web.Common.Attributes
{
    /// <summary>
    /// This attribute can be used to validate the state of a model.
    /// </summary>
    public sealed class ValidateModelAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// This method handles validation of the state of a model when an action is executing. 
        /// </summary>
        /// <param name="context">This is the context of an action filter.</param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            if (!context.ModelState.IsValid)
            {
                context.Result = new BadRequestObjectResult(context.ModelState);
            }

            base.OnActionExecuting(context);
        }
    }
}
