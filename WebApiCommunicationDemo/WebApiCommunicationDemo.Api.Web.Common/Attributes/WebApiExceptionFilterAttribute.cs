﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Web API Exception Filter Attribute
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using WebApiCommunicationDemo.Api.Web.Common.Errors;
using WebApiCommunicationDemo.Api.Web.Common.Exceptions;

namespace WebApiCommunicationDemo.Api.Web.Common.Attributes
{
    /// <summary>
    /// This attribute can be used to filter exceptions that occur within a web API.
    /// </summary>
    public sealed class WebApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// This method handles exceptions when an action is executing. 
        /// </summary>
        /// <param name="context">This is the context of an action filter.</param>
        public override void OnException(ExceptionContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            var statusCode = 500;

            var exception = context.Exception;

            var webApiError = new WebApiError();

            if (exception is WebApiException)
            {
                var webApiException = exception as WebApiException;

                context.Exception = null;

                webApiError = new WebApiError(webApiException.Message)
                {
                    Errors = webApiException.Errors,
                    Detail = webApiException.Detail
                };

                statusCode = ((WebApiException)exception).StatusCode;
            }

            var actionResult = statusCode switch
            {
                400 => new BadRequestObjectResult(webApiError),
                _ => new ObjectResult(webApiError)
            };

            context.HttpContext.Response.StatusCode = statusCode;

            context.Result = actionResult;

            base.OnException(context);
        }
    }
}
