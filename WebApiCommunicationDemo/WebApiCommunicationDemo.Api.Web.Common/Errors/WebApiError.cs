﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Web API Error Model
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System.Collections.Generic;

namespace WebApiCommunicationDemo.Api.Web.Common.Errors
{
    /// <summary>
    /// This is a model of an error that occurred in a web API.
    /// </summary>
    public class WebApiError
    {
        /// <summary>
        /// This is a general description of the error.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// This is a specific description of the error.
        /// </summary>
        public string Detail { get; set; }

        /// <summary>
        /// This specifies the severity of the error.
        /// </summary>
        public bool IsError { get; set; }

        /// <summary>
        /// This is a classification of the error.
        /// </summary>
        public string Classification { get; set; }

        /// <summary>
        /// This is a list of messages that were collected before this error was created.
        /// </summary>
        public IEnumerable<string> Errors { get; set; }

        /// <summary>
        /// This constructor can be used to intialize a <see cref="WebApiError"/>.
        /// </summary>
        /// <param name="message">This is a general description of the error.</param>
        public WebApiError(string message = "An unknown issue was encountered.")
        {
            Message = message;
            IsError = true;
        }
    }
}
