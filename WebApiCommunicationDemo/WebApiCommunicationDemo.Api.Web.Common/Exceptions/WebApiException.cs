﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Web API Exception
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System;
using System.Collections.Generic;

namespace WebApiCommunicationDemo.Api.Web.Common.Exceptions
{
    /// <summary>
    /// This exception can be thrown when general errors are encountered in a Web API.
    /// </summary>
    public class WebApiException : Exception
    {
        /// <summary>
        /// This is an HTTP status code.
        /// </summary>
        public short StatusCode { get; set; }

        /// <summary>
        /// This is a specific description of the exception.
        /// </summary>
        public string Detail { get; set; }

        /// <summary>
        /// This is a list of messages that were collected before this error was created.
        /// </summary>
        public IEnumerable<string> Errors { get; set; }

        /// <summary>
        /// This constructor can be used to initialize a <see cref="WebApiException"/>.
        /// </summary>
        public WebApiException()
        {
        }

        /// <summary>
        /// This constructor can be used to initialize a <see cref="WebApiException"/>.
        /// </summary>
        /// <param name="message">This is a message that describes the exception.</param>
        public WebApiException(string message) : base(message)
        {
        }

        /// <summary>
        /// This constructor can be used to initialize a <see cref="WebApiException"/>.
        /// </summary>
        /// <param name="message">This is a message that describes the exception.</param>
        /// <param name="innerException">This is an exception that caused a <see cref="WebApiException"/> to be thrown.</param>
        public WebApiException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// This constructor can be used to initialize a <see cref="WebApiException"/>.
        /// </summary>
        /// <param name="message">This is a general description of the exception.</param>
        /// <param name="detail">This is a specific description of the exception.</param>
        /// <param name="statusCode">This is an HTTP status code.</param>
        /// <param name="errors">This is a collection of errors that took place before an exception of this type was thrown.</param>
        public WebApiException(string message, string detail = null, short statusCode = 500, IEnumerable<string> errors = null) : base(message)
        {
            StatusCode = statusCode;
            Detail = detail;
            Errors = errors;
        }
    }
}
