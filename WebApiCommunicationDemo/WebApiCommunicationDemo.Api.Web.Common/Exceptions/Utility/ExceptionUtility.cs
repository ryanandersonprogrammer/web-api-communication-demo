﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Exception Utility
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.Extensions.Logging;
using System;

namespace WebApiCommunicationDemo.Api.Web.Common.Exceptions.Utility
{
    /// <summary>
    /// This is a utility class that contains a method for handling an exception.
    /// </summary>
    public static class ExceptionUtility
    {
        /// <summary>
        /// This method can be used to log exceptions that occur within a web API.
        /// </summary>
        /// <typeparam name="TException">This is a <see cref="WebApiException"/>.</typeparam>
        /// <param name="logger">This is a logger that records the exception.</param>
        /// <param name="exception">This is the exception that occurred within a web API.</param>
        /// <returns>This method returns the original exception so that it can be thrown.</returns>
        public static TException GetLoggedWebApiException<TException>(ILogger logger, TException exception) where TException : WebApiException
        {
            if (exception == null)
                throw new ArgumentNullException(nameof(exception));

            logger.LogError(exception, $"{exception.Message}...");
            logger.LogError($"{exception.Detail}...");

            return exception;
        }
    }
}
