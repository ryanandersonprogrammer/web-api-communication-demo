﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Intermediary Service Collection Extensions
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Globalization;
using WebApiCommunicationDemo.Api.Web.Service;
using WebApiCommunicationDemo.Api.Web.Service.Clients;
using WebApiCommunicationDemo.Api.Web.Service.Models;
using WebApiCommunicationDemo.Api.Web.Service.Options;
using WebApiCommunicationDemo.Api.Web.Service.Providers;

namespace WebApiCommunicationDemo.Extensions.DependencyInjection
{
    /// <summary>
    /// These are extension methods for setting up an intermediary service in an <see cref="IServiceCollection"/>.
    /// </summary>
    public static class IntermediaryServiceCollectionExtensions
    {
        /// <summary>
        /// This extension adds an intermediary service to the specified <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">Services will be added to this <see cref="IServiceCollection"/>.</param>
        /// <returns>The specified <see cref="IServiceCollection"/> is returned for chaining purposes.</returns>
        public static IServiceCollection AddIntermediary(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services.TryAdd(ServiceDescriptor.Transient<IIntermediaryService, DefaultIntermediaryService>());
            services.TryAdd(ServiceDescriptor.Transient<IIntermediarySecurityProvider, DefaultIntermediarySecurityProvider>());
            services.TryAdd(ServiceDescriptor.Transient<IThirdPartyClient, DefaultThirdPartyClient>());
            services.TryAdd(ServiceDescriptor.Transient<ICommunicationStringsProvider, DefaultCommunicationStringsProvider>());

            services.AddLocalization();

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var cultures = new[]
                {
                   new CultureInfo("en-US")
                };

                options.DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("en-US", "en-US");

                options.SupportedCultures = cultures;
                options.SupportedUICultures = cultures;
            });

            services.AddDbContext<IntermediaryCallbackStateContext>(options => options.UseInMemoryDatabase("IntermediaryCallbackState"), ServiceLifetime.Transient);

            return services;
        }

        /// <summary>
        /// This extension adds an intermediary service to the specified <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">Services will be added to this <see cref="IServiceCollection"/>.</param>
        /// <param name="configure">This is an action delegate for configuring <see cref="IntermediaryOptions"/>.</param>
        /// <returns>The specified <see cref="IServiceCollection"/> is returned for chaining purposes.</returns>
        public static IServiceCollection AddIntermediary(this IServiceCollection services, Action<IntermediaryOptions> configure)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (configure != null)
                services.Configure(configure);

            return services.AddIntermediary();
        }
    }
}
