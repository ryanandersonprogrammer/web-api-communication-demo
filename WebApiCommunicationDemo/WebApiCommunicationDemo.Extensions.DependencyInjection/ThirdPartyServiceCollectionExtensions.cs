﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Third Party Service Collection Extensions
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using WebApiCommunicationDemo.Api.Web.Service;
using WebApiCommunicationDemo.Api.Web.Service.Clients;
using WebApiCommunicationDemo.Api.Web.Service.Managers;
using WebApiCommunicationDemo.Api.Web.Service.Options;
using WebApiCommunicationDemo.Api.Web.Service.Providers;

namespace WebApiCommunicationDemoExtensions.DependencyInjection
{
    /// <summary>
    /// These are extension methods for setting up a third-party service in an <see cref="IServiceCollection"/>.
    /// </summary>
    public static class ThirdPartyServiceCollectionExtensions
    {
        /// <summary>
        /// This extension adds a third-party service to the specified <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">Services will be added to this <see cref="IServiceCollection"/>.</param>
        /// <returns>The specified <see cref="IServiceCollection"/> is returned for chaining purposes.</returns>
        public static IServiceCollection AddThirdParty(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services.TryAdd(ServiceDescriptor.Transient<IIntermediaryClient, DefaultIntermediaryClient>());
            services.TryAdd(ServiceDescriptor.Transient<IThirdPartyService, DefaultThirdPartyService>());
            services.TryAdd(ServiceDescriptor.Singleton<IThirdPartyCallbackManager, DefaultThirdPartyCallbackManager>());
            services.TryAdd(ServiceDescriptor.Transient<IThirdPartySecurityProvider, DefaultThirdPartySecurityProvider>());
            services.TryAdd(ServiceDescriptor.Transient<ICommunicationStringsProvider, DefaultCommunicationStringsProvider>());

            services.AddLocalization();

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var cultures = new[]
                {
                   new CultureInfo("en-US")
                };

                options.DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("en-US", "en-US");

                options.SupportedCultures = cultures;
                options.SupportedUICultures = cultures;
            });

            return services;
        }

        /// <summary>
        /// This extension adds a third-party service to the specified <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">Services will be added to this <see cref="IServiceCollection"/>.</param>
        /// <param name="configure">This is an action delegate for configuring <see cref="ThirdPartyOptions"/>.</param>
        /// <returns>The specified <see cref="IServiceCollection"/> is returned for chaining purposes.</returns>
        public static IServiceCollection AddThirdParty(this IServiceCollection services, Action<ThirdPartyOptions> configure)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (configure != null)
                services.Configure(configure);

            return services.AddThirdParty();
        }
    }
}
