﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Third Party Service Interface
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Service.Models;

namespace WebApiCommunicationDemo.Api.Web.Service
{
    /// <summary>
    /// This interface is a contract for a service that communicates with an intermediary API.
    /// </summary>
    public interface IThirdPartyService
    {
        /// <summary>
        /// This method is used to start processing information through a provided callback route.
        /// </summary>
        /// <param name="intermediaryCallbackPayload">This payload contains a callback route and information to be processed by a third-party web API.</param>
        /// <returns>This method returns a task.</returns>
        Task StartAsync(IntermediaryCallbackPayload intermediaryCallbackPayload);
    }
}
