﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Intermediary Client Default Implementation
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Service.Models;
using WebApiCommunicationDemo.Api.Web.Service.Options;
using WebApiCommunicationDemo.Api.Web.Service.Providers;

namespace WebApiCommunicationDemo.Api.Web.Service.Clients
{
    /// <summary>
    /// This is the default implementation of <see cref="IIntermediaryClient"/>.
    /// </summary>
    public class DefaultIntermediaryClient : IIntermediaryClient
    {
        private readonly IThirdPartySecurityProvider _thirdPartySecurityProvider;

        private readonly ThirdPartyOptions _thirdPartyOptions;

        /// <summary>
        /// This constructor can be used to initialize a <see cref="DefaultIntermediaryClient"/>.
        /// </summary>
        /// <param name="thirdPartySecurityProvider">This is the <see cref="IThirdPartySecurityProvider"/> that is used to provide security information.</param>
        /// <param name="thirdPartyOptions">These are the <see cref="ThirdPartyOptions"/> that are used to configure a service for communicating with an intermediary API.</param>
        public DefaultIntermediaryClient(IThirdPartySecurityProvider thirdPartySecurityProvider, IOptions<ThirdPartyOptions> thirdPartyOptions)
        {
            if (thirdPartyOptions == null)
                throw new ArgumentNullException(nameof(thirdPartyOptions));

            _thirdPartySecurityProvider = thirdPartySecurityProvider ?? throw new ArgumentNullException(nameof(thirdPartySecurityProvider));

            _thirdPartyOptions = thirdPartyOptions.Value;
        }

        /// <summary>
        /// This method can be used to send a request to an intermediary API to start the processing of information.
        /// </summary>
        /// <param name="status">This parameter is used to indicate the status of work being performed.</param>
        /// <param name="callbackRouteUri">This is a URI for a callback route.</param>
        /// <returns>This method returns a task of an <see cref="HttpResponseMessage"/>.</returns>
        public async Task<HttpResponseMessage> StartAsync(string status, Uri callbackRouteUri)
        {
            return await SendRequestAsync(HttpMethod.Post, callbackRouteUri, status, MediaTypes.RawText).ConfigureAwait(false);
        }

        /// <summary>
        /// This method can be used to send a request to an intermediary API to process information through a callback route.
        /// </summary>
        /// <param name="intermediaryCallbackState">This is a payload that contains information about the status of work being performed.</param>
        /// <param name="callbackRouteUri">This is a URI for a callback route.</param>
        /// <returns>This method returns a task of an <see cref="HttpResponseMessage"/>.</returns>
        public async Task<HttpResponseMessage> ProcessAsync(IntermediaryCallbackState intermediaryCallbackState, Uri callbackRouteUri)
        {
            return await SendRequestAsync(HttpMethod.Put, callbackRouteUri, JsonSerializer.Serialize(intermediaryCallbackState), MediaTypes.Json).ConfigureAwait(false);
        }

        private async Task<HttpResponseMessage> SendRequestAsync(HttpMethod httpMethod, Uri requestUri, string stringContent, string mediaType)
        {
            using HttpClient httpClient = new HttpClient
            {
                BaseAddress = _thirdPartyOptions.IntermediaryBaseAddress
            };

            var authenticationRequest = await _thirdPartySecurityProvider.GetAuthenticationRequestAsync().ConfigureAwait(false);

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authenticationRequest.Scheme, authenticationRequest.GetParameter());

            using HttpRequestMessage request = new HttpRequestMessage
            {
                Content = new StringContent(stringContent, Encoding.UTF8, mediaType),
                Method = httpMethod,
                RequestUri = requestUri
            };

            return await httpClient.SendAsync(request).ConfigureAwait(false);
        }
    }
}
