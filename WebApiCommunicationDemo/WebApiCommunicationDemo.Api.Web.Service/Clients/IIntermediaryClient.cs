﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Intermediary Client Interface
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System;
using System.Net.Http;
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Service.Models;

namespace WebApiCommunicationDemo.Api.Web.Service.Clients
{
    /// <summary>
    /// This client is used to communicate with an intermediary API.
    /// </summary>
    public interface IIntermediaryClient
    {
        /// <summary>
        /// This method can be used to send a request to an intermediary API to start the processing of information.
        /// </summary>
        /// <param name="status">This parameter is used to indicate the status of work being performed.</param>
        /// <param name="callbackRouteUri">This is a URI for a callback route.</param>
        /// <returns>This method returns a task of an <see cref="HttpResponseMessage"/>.</returns>
        Task<HttpResponseMessage> StartAsync(string status, Uri callbackRouteUri);

        /// <summary>
        /// This method can be used to send a request to an intermediary API to process information through a callback route.
        /// </summary>
        /// <param name="intermediaryCallbackState">This is a payload that contains information about the status of work being performed.</param>
        /// <param name="callbackRouteUri">This is a URI for a callback route.</param>
        /// <returns>This method returns a task of an <see cref="HttpResponseMessage"/>.</returns>
        Task<HttpResponseMessage> ProcessAsync(IntermediaryCallbackState intermediaryCallbackState, Uri callbackRouteUri);
    }
}
