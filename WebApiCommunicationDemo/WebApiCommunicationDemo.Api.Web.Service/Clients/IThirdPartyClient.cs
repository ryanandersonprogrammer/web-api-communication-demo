﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Third Party Client Interface
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System.Net.Http;
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Service.Models;

namespace WebApiCommunicationDemo.Api.Web.Service.Clients
{
    /// <summary>
    /// This client is used to communicate with a third-party web API.
    /// </summary>
    public interface IThirdPartyClient
    {
        /// <summary>
        /// This method can be used to send a request to a third-party web API to start the processing of information.
        /// </summary>
        /// <param name="intermediaryCallbackPayload">This payload contains information that is used by a third-party web API to begin processing.</param>
        /// <returns>This method returns a task of an <see cref="HttpResponseMessage"/>.</returns>
        Task<HttpResponseMessage> SendRequestToThirdPartyToStartProcessingAsync(IntermediaryCallbackPayload intermediaryCallbackPayload);
    }
}
