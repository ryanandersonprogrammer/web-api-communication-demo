﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Third Party Client Default Implementation
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Service.Models;
using WebApiCommunicationDemo.Api.Web.Service.Options;
using WebApiCommunicationDemo.Api.Web.Service.Providers;

namespace WebApiCommunicationDemo.Api.Web.Service.Clients
{
    /// <summary>
    /// This is the default implementation of <see cref="IThirdPartyClient"/>.
    /// </summary>
    public class DefaultThirdPartyClient : IThirdPartyClient
    {
        private readonly IIntermediarySecurityProvider _intermediarySecurityProvider;

        private readonly IntermediaryOptions _intermediaryOptions;

        /// <summary>
        /// This constructor can be used to initialize a <see cref="DefaultThirdPartyClient"/>.
        /// </summary>
        /// <param name="intermediarySecurityProvider">This is the <see cref="IIntermediarySecurityProvider"/> that is used to provide security information.</param>.
        /// <param name="intermediaryOptions">These are the <see cref="IntermediaryOptions"/> that can be used to configure a service for communicating with a third-party web API.</param>
        public DefaultThirdPartyClient(IIntermediarySecurityProvider intermediarySecurityProvider, IOptions<IntermediaryOptions> intermediaryOptions)
        {
            if (intermediaryOptions == null)
                throw new ArgumentNullException(nameof(intermediaryOptions));

            _intermediarySecurityProvider = intermediarySecurityProvider ?? throw new ArgumentNullException(nameof(intermediarySecurityProvider));
            _intermediaryOptions = intermediaryOptions.Value;
        }

        /// <summary>
        /// This method can be used to send a request to a third-party web API to start the processing of information.
        /// </summary>
        /// <param name="intermediaryCallbackPayload">This payload contains information that is used by a third-party web API to begin processing.</param>
        /// <returns>This method returns a task of an <see cref="HttpResponseMessage"/>.</returns>
        public async Task<HttpResponseMessage> SendRequestToThirdPartyToStartProcessingAsync(IntermediaryCallbackPayload intermediaryCallbackPayload)
        {
            using HttpClient httpClient = new HttpClient
            {
                BaseAddress = _intermediaryOptions.ThirdPartyStubBaseAddress
            };

            var authenticationRequest = await _intermediarySecurityProvider.GetRequestAuthenticationAsync().ConfigureAwait(false);

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authenticationRequest.Scheme, authenticationRequest.GetParameter());

            using var stringContent = new StringContent(JsonSerializer.Serialize(intermediaryCallbackPayload), Encoding.UTF8, MediaTypes.Json);

            var requestUri = new Uri($"{_intermediaryOptions.ThirdPartyStubBaseRequestUri.OriginalString}/{_intermediaryOptions.ThirdPartyStubRequestUri.OriginalString}", UriKind.Relative);

            return await httpClient.PostAsync(requestUri, stringContent).ConfigureAwait(false);
        }
    }
}
