﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Intermediary Service Interface
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Service.Models;

namespace WebApiCommunicationDemo.Api.Web.Service
{
    /// <summary>
    /// This interface is a contract for a service that communicates with a third-party web API.
    /// </summary>
    public interface IIntermediaryService
    {
        /// <summary>
        /// This method is used to retrieve statuses based on a unique identifier.
        /// </summary>
        /// <param name="id">This is a unique identifier of a record that contains information about work being performed.</param>
        /// <returns>This method returns a task of a callback state.</returns>
        Task<IntermediaryCallbackState> RetrieveStatusResponseAsync(string id);

        /// <summary>
        /// This method is used to send an initial request to a third-party web API to start the processing of information.
        /// </summary>
        /// <param name="intermediaryCallbackState">This payload contains information that is used by a third-party web API to start the processing of information.</param>
        /// <returns>This method returns a task of an identifier that can be used to identify a status record.</returns
        Task<string> SendInitialRequestAsync(IntermediaryCallbackState intermediaryCallbackState);

        /// <summary>
        /// A third-party web API will send a callback request to this endpoint to indicate that the processing of information has started.
        /// </summary>
        /// <param name="text">This is a status from a third-party web API.</param>
        /// <returns>This method returns a task.</returns>
        Task ReceiveInitialRequestFromThirdPartyServiceAsync(string text);

        /// <summary>
        /// A third-party web API will send a callback request to this endpoint with a status about the processing of information.
        /// </summary>
        /// <param name="intermediaryCallbackState">This payload contains information for updating a status record.</param>
        /// <returns>This method returns a task.</returns>
        Task UpdateStateAsync(IntermediaryCallbackState intermediaryCallbackState);
    }
}
