﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Third Party Options
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System;
using WebApiCommunicationDemo.Api.Web.Security.Models;

namespace WebApiCommunicationDemo.Api.Web.Service.Options
{
    /// <summary>
    /// These options can be used to configure an intermediary service to communicate with an intermediary API.
    /// </summary>
    public class ThirdPartyOptions
    {
        /// <summary>
        /// This property is used to identify a third-party web API as a client.
        /// </summary>
        public string IntermediaryClientIdentifier { get; set; }

        /// <summary>
        /// This is a secret that is shared with an intermediary API to gain access to resources.
        /// </summary>
        public string IntermediaryClientSecret { get; set; }

        /// <summary>
        /// This is the base address of a target server.
        /// </summary>
        public Uri IntermediaryBaseAddress { get; set; }

        /// <summary>
        /// This is a relative path that is the base of all routes to resources hosted at a target web API.
        /// </summary>
        public Uri IntermediaryBaseRequestUri { get; set; }

        /// <summary>
        /// This is the number of times that processing will occur.
        /// </summary>
        public int NumberOfCallbackRequests { get; set; }

        /// <summary>
        /// This is the amount of time that will elapse between callback requests.
        /// </summary>
        public TimeSpan AmountOfTimeBetweenCallbackRequests { get; set; }

        /// <summary>
        /// This is information that will be used to respond to a request from an intermediary web API with a challenge.
        /// </summary>
        public BasicHttpAuthenticationResponse StartProcessingResourceAuthentication { get; set; }

        /// <summary>
        /// This is used to send an authorization request with "Basic" HTTP authentication.
        /// </summary>
        public BasicHttpAuthenticationRequest CallbackResourceAuthentication { get; set; }

        /// <summary>
        /// This is used to determine whether status updates should be sent after attempting to complete the processing of information.
        /// </summary>
        public bool ContinueUpdatingAfterAttemptingToComplete { get; set; }
    }
}
