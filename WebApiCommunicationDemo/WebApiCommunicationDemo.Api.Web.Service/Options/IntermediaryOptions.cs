﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Intermediary Options
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System;
using WebApiCommunicationDemo.Api.Web.Security.Models;

namespace WebApiCommunicationDemo.Api.Web.Service.Options
{
    /// <summary>
    /// These options can be used to configure an intermediary service to communicate with a third-party web API.
    /// </summary>
    public class IntermediaryOptions
    {
        /// <summary>
        /// This is the server address of a third-party web API.
        /// </summary>
        public Uri ThirdPartyStubBaseAddress { get; set; }

        /// <summary>
        /// This is a relative path that is the base of all routes to resources hosted at a target web API.
        /// </summary>
        public Uri ThirdPartyStubBaseRequestUri { get; set; }

        /// <summary>
        /// This is the request URI for sending requests to process information.
        /// </summary>
        public Uri ThirdPartyStubRequestUri { get; set; }

        /// <summary>
        /// Callback requests from a third-party web API must be made within this time frame.
        /// </summary>
        public double WaitDurationInDays { get; set; }

        /// <summary>
        /// Callback requests from a third-party web API will be sent to this route.
        /// </summary>
        public string CallbackRoute { get; set; }

        /// <summary>
        /// This is information that will be used to respond to a third-party web API with a challenge.
        /// </summary>
        public BasicHttpAuthenticationResponse CallbackResourceAuthentication { get; set; }

        /// <summary>
        /// This is used to send an authorization request with "Basic" HTTP authentication.
        /// </summary>
        public BasicHttpAuthenticationRequest StartProcessingResourceAuthentication { get; set; }
    }
}
