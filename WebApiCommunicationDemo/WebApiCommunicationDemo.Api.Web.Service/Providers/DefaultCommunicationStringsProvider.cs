﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Communication Strings Provider Default Implementation
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.Extensions.Localization;

namespace WebApiCommunicationDemo.Api.Web.Service.Providers
{
    /// <summary>
    /// This is the default implementation of <see cref="ICommunicationStringsProvider"/>.
    /// </summary>
    public class DefaultCommunicationStringsProvider : ICommunicationStringsProvider
    {
        private readonly IStringLocalizer<DefaultCommunicationStringsProvider> _stringLocalizer;

        /// <summary>
        /// This constructor can be used to initialize a <see cref="DefaultCommunicationStringsProvider"/>.
        /// </summary>
        /// <param name="stringLocalizer">This is used to access localized strings from a resource file.</param>
        public DefaultCommunicationStringsProvider(IStringLocalizer<DefaultCommunicationStringsProvider> stringLocalizer)
        {
            _stringLocalizer = stringLocalizer;
        }

        /// <summary>
        /// This localized string is used by an intermediary service to log a general description of an error that was encountered while attempting to update a record.
        /// </summary>
        public LocalizedString IntermediaryApiRecordUpdateGeneralError => Get(nameof(IntermediaryApiRecordUpdateGeneralError));

        /// <summary>
        /// This localized string is used by an intermediary service to log a specific description of an error that was encountered while attempting to update a record.
        /// </summary>
        public LocalizedString IntermediaryApiSingleEntityUpdateValidationError => Get(nameof(IntermediaryApiSingleEntityUpdateValidationError));

        /// <summary>
        /// This localized string is used by an intermediary service to log a specific description of an error that was encountered while attempting to update multiple records.
        /// </summary>
        public LocalizedString IntermediaryApiMultipleEntitiesUpdateValidationError => Get(nameof(IntermediaryApiMultipleEntitiesUpdateValidationError));

        /// <summary>
        /// This localized string is used by an intermediary service to log an attempt to update a record outside of an allotted time frame.
        /// </summary>
        public LocalizedString IntermediaryApiCallbackStatusUpdateAllottedTimeFrameError => Get(nameof(IntermediaryApiCallbackStatusUpdateAllottedTimeFrameError));

        /// <summary>
        /// This localized string is used by an intermediary service to log an unexpected error that takes place during a callback.
        /// </summary>
        public LocalizedString IntermediaryApiCallbackUnexpectedError => Get(nameof(IntermediaryApiCallbackUnexpectedError));

        /// <summary>
        /// This localized string is used by an intermediary service to log a general description of an error that took place during an initial request to start processing information.
        /// </summary>
        public LocalizedString IntermediaryApiInitialRequestError => Get(nameof(IntermediaryApiInitialRequestError));

        /// <summary>
        /// This localized string is used by an intermediary service to log information before attempting to iteratively update records.
        /// </summary>
        public LocalizedString IntermediaryApiBeforeIterativeUpdateInformation => Get(nameof(IntermediaryApiBeforeIterativeUpdateInformation));

        /// <summary>
        /// This localized string is used by an intermediary service to log an error when an attempt to update a status fails.
        /// </summary>
        public LocalizedString IntermediaryApiStatusUpdateError => Get(nameof(IntermediaryApiStatusUpdateError));

        /// <summary>
        /// This localized string is used by an intermediary service to log information before saving changes that were made to a database context.
        /// </summary>
        public LocalizedString IntermediaryApiBeforeSaveInformation => Get(nameof(IntermediaryApiBeforeSaveInformation));

        /// <summary>
        /// This localized string is used by an intermediary service to log an error that was encountered while attempting to retrieve information about processing.
        /// </summary>
        public LocalizedString IntermediaryApiCallbackStateRetrievalError => Get(nameof(IntermediaryApiCallbackStateRetrievalError));

        /// <summary>
        /// This localized string is used by an intermediary service to log an error when the third-party web API doesn't respond with a success status code during an initial request.
        /// </summary>
        public LocalizedString IntermediaryApiInitialRequestIsNotSuccessStatusCode => Get(nameof(IntermediaryApiInitialRequestIsNotSuccessStatusCode));

        /// <summary>
        /// This localized string is used by an intermediary service to log an error when an invalid identifier is used to retrieve information about processing.
        /// </summary>
        public LocalizedString IntermediaryApiStatusRequestIdentifierError => Get(nameof(IntermediaryApiStatusRequestIdentifierError));

        /// <summary>
        /// This localized string is used by an intermediary service to log information at the beginning of an initial request.
        /// </summary>
        public LocalizedString IntermediaryApiInitialRequestReceivingHttpPostInformation => Get(nameof(IntermediaryApiInitialRequestReceivingHttpPostInformation));

        /// <summary>
        /// This localized string is used by an intermediary service to log information at the beginning of a request that was made through a callback route to indicate a status after processing begins.
        /// </summary>
        public LocalizedString IntermediaryApiCallbackReceivingHttpPostInformation => Get(nameof(IntermediaryApiCallbackReceivingHttpPostInformation));

        /// <summary>
        /// This localized string is used by an intermediary service to log information at the beginning of a request that was made through a callback route to indicate a status during processing.
        /// </summary>
        public LocalizedString IntermediaryApiCallbackReceivingHttpPutInformation => Get(nameof(IntermediaryApiCallbackReceivingHttpPutInformation));

        /// <summary>
        /// This localized string is used by a third-party service to log an error when processing cannot begin.
        /// </summary>
        public LocalizedString ThirdPartyApiCallbackCannotStartProcessingError => Get(nameof(ThirdPartyApiCallbackCannotStartProcessingError));

        /// <summary>
        /// This localized string is used by a third-party service to log information at the beginning of a request to start processing information.
        /// </summary>
        public LocalizedString ThirdPartyApiHttpPostReceivingRequestInformation => Get(nameof(ThirdPartyApiHttpPostReceivingRequestInformation));

        /// <summary>
        /// This localized string is used by a third-party service to log an error when an invalid body is provided.
        /// </summary>
        public LocalizedString ThirdPartyApiCallbackInvalidBodyError => Get(nameof(ThirdPartyApiCallbackInvalidBodyError));

        /// <summary>
        /// This localized string is used to log an expected error.
        /// </summary>
        public LocalizedString GeneralWebApiExpectedIssueError => Get(nameof(GeneralWebApiExpectedIssueError));

        /// <summary>
        /// This localized string is used to log an unexpected error.
        /// </summary>
        public LocalizedString GeneralWebApiUnexpectedIssueError => Get(nameof(GeneralWebApiUnexpectedIssueError));

        private LocalizedString Get(string name) => _stringLocalizer.GetString(name);
    }
}
