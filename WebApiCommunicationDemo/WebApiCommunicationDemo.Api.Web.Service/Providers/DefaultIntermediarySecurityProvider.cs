﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Intermediary Security Provider Default Implementation
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Security.Models;
using WebApiCommunicationDemo.Api.Web.Service.Options;

namespace WebApiCommunicationDemo.Api.Web.Service.Providers
{
    /// <summary>
    /// This is the default implementation of <see cref="IIntermediarySecurityProvider"/>.
    /// </summary>
    public class DefaultIntermediarySecurityProvider : IIntermediarySecurityProvider
    {
        private readonly IntermediaryOptions _intermediaryOptions;

        /// <summary>
        /// This constructor can be used to initialize a <see cref="DefaultIntermediarySecurityProvider"/>.
        /// </summary>
        /// <param name="intermediaryOptions">These are the <see cref="IntermediaryOptions"/> that are used to configure a service for communication with a third-party web API.</param>
        public DefaultIntermediarySecurityProvider(IOptions<IntermediaryOptions> intermediaryOptions)
        {
            if (intermediaryOptions == null)
                throw new ArgumentNullException(nameof(intermediaryOptions));

            _intermediaryOptions = intermediaryOptions.Value;
        }

        /// <summary>
        /// This method retrieves information that is used to access a resource with "Basic" HTTP Authentication.
        /// </summary>
        /// <returns>This method returns a task of a <see cref="BasicHttpAuthenticationRequest"/>.</returns>
        public Task<BasicHttpAuthenticationRequest> GetRequestAuthenticationAsync()
        {
            return Task.FromResult(_intermediaryOptions.StartProcessingResourceAuthentication);
        }

        /// <summary>
        /// This method retrieves information that is used when a client attempts to access a resource with "Basic" HTTP Authentication.
        /// </summary>
        /// <returns>This method returns a task of a <see cref="BasicHttpAuthenticationResponse"/>.</returns>
        public Task<BasicHttpAuthenticationResponse> GetResponseAuthenticationAsync()
        {
            return Task.FromResult(_intermediaryOptions.CallbackResourceAuthentication);
        }
    }
}
