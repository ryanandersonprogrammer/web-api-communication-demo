﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Third Party Security Provider Interface
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Security.Models;

namespace WebApiCommunicationDemo.Api.Web.Service.Providers
{
    /// <summary>
    /// This provider can be used to retrieve information that is used to gain access to a resource of an intermediary API.
    /// </summary>
    public interface IThirdPartySecurityProvider
    {
        /// <summary>
        /// This method can be used to retrieve information for an authorization request.
        /// </summary>
        /// <returns>This method returns a task of a <see cref="BasicHttpAuthenticationRequest"/>.</returns>
        Task<BasicHttpAuthentication> GetAuthenticationAsync();

        /// <summary>
        /// This method is used to retrieve arguments for creating an authorization string.
        /// </summary>
        /// <returns>This method returns a task of a <see cref="BasicHttpAuthentication"/>.</returns>
        Task<BasicHttpAuthenticationRequest> GetAuthenticationRequestAsync();

        /// <summary>
        /// This method can be used to retrieve a header value for authorizing with "Basic" HTTP Authentication.
        /// </summary>
        /// <returns>This method returns a task of a string that can be used in an authorization header.</returns>
        Task<string> GetAuthorizationHeaderValueAsync();

        /// <summary>
        /// This method can be used to retrieve the parameter of an authorization header.
        /// </summary>
        /// <returns>This method returns a task of a string that can be used as the parameter of an authorization header.</returns>
        Task<string> GetAuthorizationParameterAsync();
    }
}
