﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Communication Strings Provider Interface
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.Extensions.Localization;

namespace WebApiCommunicationDemo.Api.Web.Service.Providers
{
    /// <summary>
    /// This is a contract for a provider that accesses localized logging information from a resource file.
    /// </summary>
    public interface ICommunicationStringsProvider
    {
        /// <summary>
        /// This localized string is used by an intermediary service to log a general description of an error that was encountered while attempting to update a record.
        /// </summary>
        LocalizedString IntermediaryApiRecordUpdateGeneralError { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log a specific description of an error that was encountered while attempting to update a record.
        /// </summary>
        LocalizedString IntermediaryApiSingleEntityUpdateValidationError { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log a specific description of an error that was encountered while attempting to update multiple records.
        /// </summary>
        LocalizedString IntermediaryApiMultipleEntitiesUpdateValidationError { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log an attempt to update a record outside of an allotted time frame.
        /// </summary>
        LocalizedString IntermediaryApiCallbackStatusUpdateAllottedTimeFrameError { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log an unexpected error that takes place during a callback.
        /// </summary>
        LocalizedString IntermediaryApiCallbackUnexpectedError { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log a general description of an error that took place during an initial request to start processing information.
        /// </summary>
        LocalizedString IntermediaryApiInitialRequestError { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log information before attempting to iteratively update records.
        /// </summary>
        LocalizedString IntermediaryApiBeforeIterativeUpdateInformation { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log an error when an attempt to update a status fails.
        /// </summary>
        LocalizedString IntermediaryApiStatusUpdateError { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log information before saving changes that were made to a database context.
        /// </summary>
        LocalizedString IntermediaryApiBeforeSaveInformation { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log an error that was encountered while attempting to retrieve information about processing.
        /// </summary>
        LocalizedString IntermediaryApiCallbackStateRetrievalError { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log an error when a third-party web API doesn't respond with a success status code during an initial request.
        /// </summary>
        LocalizedString IntermediaryApiInitialRequestIsNotSuccessStatusCode { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log an error when an invalid identifier is used to retrieve information about processing.
        /// </summary>
        LocalizedString IntermediaryApiStatusRequestIdentifierError { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log information at the beginning of an initial request.
        /// </summary>
        LocalizedString IntermediaryApiInitialRequestReceivingHttpPostInformation { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log information at the beginning of a request that was made through a callback route to indicate a status after processing begins.
        /// </summary>
        LocalizedString IntermediaryApiCallbackReceivingHttpPostInformation { get; }

        /// <summary>
        /// This localized string is used by an intermediary service to log information at the beginning of a request that was made through a callback route to indicate a status during processing.
        /// </summary>
        LocalizedString IntermediaryApiCallbackReceivingHttpPutInformation { get; }

        /// <summary>
        /// This localized string is used by a third-party service to log an error when processing cannot begin.
        /// </summary>
        LocalizedString ThirdPartyApiCallbackCannotStartProcessingError { get; }

        /// <summary>
        /// This localized string is used by a third-party service to log information at the beginning of a request to start processing information.
        /// </summary>
        LocalizedString ThirdPartyApiHttpPostReceivingRequestInformation { get; }

        /// <summary>
        /// This localized string is used by a third-party service to log an error when an invalid body is provided.
        /// </summary>
        LocalizedString ThirdPartyApiCallbackInvalidBodyError { get; }

        /// <summary>
        /// This localized string is used to log an expected error.
        /// </summary>
        LocalizedString GeneralWebApiExpectedIssueError { get; }

        /// <summary>
        /// This localized string is used to log an unexpected error.
        /// </summary>
        LocalizedString GeneralWebApiUnexpectedIssueError { get; }
    }
}
