﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Intermediary Security Provider Interface
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Security.Models;

namespace WebApiCommunicationDemo.Api.Web.Service.Providers
{
    /// <summary>
    /// This is a contract for a provider of security information that is used by an intermediary API.
    /// </summary>
    public interface IIntermediarySecurityProvider
    {
        /// <summary>
        /// This method retrieves information that is used to access a resource with "Basic" HTTP Authentication.
        /// </summary>
        /// <returns>This method returns a task of a <see cref="BasicHttpAuthenticationRequest"/>.</returns>
        public Task<BasicHttpAuthenticationRequest> GetRequestAuthenticationAsync();

        /// <summary>
        /// This method retrieves information that is used when a client attempts to access a resource with "Basic" HTTP Authentication.
        /// </summary>
        /// <returns>This method returns a task of a <see cref="BasicHttpAuthenticationResponse"/>.</returns>
        Task<BasicHttpAuthenticationResponse> GetResponseAuthenticationAsync();
    }
}
