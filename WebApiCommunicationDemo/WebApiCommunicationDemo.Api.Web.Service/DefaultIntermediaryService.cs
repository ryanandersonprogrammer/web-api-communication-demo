﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Intermediary Service Default Implementation
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Common.Exceptions;
using WebApiCommunicationDemo.Api.Web.Common.Exceptions.Utility;
using WebApiCommunicationDemo.Api.Web.Service.Clients;
using WebApiCommunicationDemo.Api.Web.Service.Models;
using WebApiCommunicationDemo.Api.Web.Service.Options;
using WebApiCommunicationDemo.Api.Web.Service.Providers;

namespace WebApiCommunicationDemo.Api.Web.Service
{
    /// <summary>
    /// This is the default implementation of <see cref="IIntermediaryService"/>.
    /// </summary>
    public class DefaultIntermediaryService : IIntermediaryService
    {
        private readonly ILogger<DefaultIntermediaryService> _logger;

        private readonly IThirdPartyClient _thirdPartyClient;

        private readonly IntermediaryCallbackStateContext _intermediaryCallbackStateContext;

        private readonly IntermediaryOptions _intermediaryOptions;

        private readonly ICommunicationStringsProvider _communicationStringsProvider;

        /// <summary>
        /// This constructor can be used to initialize a <see cref="DefaultIntermediaryService"/>.
        /// </summary>
        /// <param name="logger">This is a logger that is used to record events.</param>
        /// <param name="communicationStringsProvider">This is the <see cref="ICommunicationStringsProvider"/> that provides strings for logging.</param>
        /// <param name="thirdPartyClient">This is the <see cref="IThirdPartyClient"/> that is used to communicate with a third-party web API.</param>
        /// <param name="intermediaryCallbackStateContext">This is the <see cref="IntermediaryCallbackStateContext"/> that is used to maintain statuses.</param>
        /// <param name="intermediaryOptions">These are the <see cref="IntermediaryOptions"/> that are used to configure a service for communication with a third-party web API.</param>
        public DefaultIntermediaryService(ILogger<DefaultIntermediaryService> logger, ICommunicationStringsProvider communicationStringsProvider, IThirdPartyClient thirdPartyClient, IntermediaryCallbackStateContext intermediaryCallbackStateContext, IOptions<IntermediaryOptions> intermediaryOptions)
        {
            if (intermediaryOptions == null)
                throw new ArgumentNullException(nameof(intermediaryOptions));

            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _thirdPartyClient = thirdPartyClient ?? throw new ArgumentNullException(nameof(thirdPartyClient));
            _intermediaryCallbackStateContext = intermediaryCallbackStateContext ?? throw new ArgumentNullException(nameof(intermediaryCallbackStateContext));
            _intermediaryOptions = intermediaryOptions.Value;
            _communicationStringsProvider = communicationStringsProvider ?? throw new ArgumentNullException(nameof(communicationStringsProvider));
        }

        /// <summary>
        /// This method is used to retrieve statuses based on a unique identifier.
        /// </summary>
        /// <param name="id">This is a unique identifier of a record that contains information about work being performed.</param>
        /// <returns>This method returns a task of a callback state.</returns>
        public async Task<IntermediaryCallbackState> RetrieveStatusResponseAsync(string id)
        {
            _logger.LogInformation($"Receiving through HTTP GET a request to retrieve the status for the record that is identified by id={id}...");

            if (string.IsNullOrWhiteSpace(id))
                throw ExceptionUtility.GetLoggedWebApiException(_logger, new WebApiException(_communicationStringsProvider.GeneralWebApiExpectedIssueError.Value, _communicationStringsProvider.IntermediaryApiCallbackStateRetrievalError.Value, 400, new List<string>
                {
                    _communicationStringsProvider.IntermediaryApiStatusRequestIdentifierError.Value
                }));

            _logger.LogInformation($"Retrieve from the in-memory store a callback state that is identified by id={id}...");

            var callbackState = await _intermediaryCallbackStateContext.CallbackStates.FirstOrDefaultAsync(callbackState => string.Equals(callbackState.Id.ToString(), id, StringComparison.OrdinalIgnoreCase)).ConfigureAwait(false);

            if (callbackState == default)
                throw ExceptionUtility.GetLoggedWebApiException(_logger, new WebApiException(_communicationStringsProvider.GeneralWebApiExpectedIssueError.Value, _communicationStringsProvider.IntermediaryApiCallbackStateRetrievalError.Value, 400, new List<string>
                {
                    $"The store does not contain a user with id={id}."
                }));

            return callbackState;
        }

        /// <summary>
        /// This method is used to send an initial request to a third-party web API to start the processing of information.
        /// </summary>
        /// <param name="intermediaryCallbackState">This payload contains information that is used by a third-party web API to start the processing of information.</param>
        /// <returns>This method returns a task of an identifier that can be used to identify a status record.</returns
        public async Task<string> SendInitialRequestAsync(IntermediaryCallbackState intermediaryCallbackState)
        {
            _logger.LogInformation(_communicationStringsProvider.IntermediaryApiInitialRequestReceivingHttpPostInformation.Value);

            if (intermediaryCallbackState == null)
                throw new ArgumentNullException(nameof(intermediaryCallbackState));

            var thirdPartyClientTask = _thirdPartyClient.SendRequestToThirdPartyToStartProcessingAsync(new IntermediaryCallbackPayload { Body = intermediaryCallbackState.Body, Callback = $"/{_intermediaryOptions.CallbackRoute}" });

            intermediaryCallbackState.Id = Guid.NewGuid();
            intermediaryCallbackState.CreationTimeStamp = DateTime.UtcNow;

            await _intermediaryCallbackStateContext.AddAsync(intermediaryCallbackState);

            await SaveAsync(1).ConfigureAwait(false);

            _logger.LogInformation($"A new record with id={intermediaryCallbackState.Id.ToString()} was created..");

            var response = await thirdPartyClientTask.ConfigureAwait(false);

            if (response.StatusCode != HttpStatusCode.NoContent)
                throw ExceptionUtility.GetLoggedWebApiException(_logger, new WebApiException(_communicationStringsProvider.GeneralWebApiExpectedIssueError.Value, _communicationStringsProvider.IntermediaryApiInitialRequestError.Value, 400, new List<string>
                {
                    _communicationStringsProvider.IntermediaryApiInitialRequestIsNotSuccessStatusCode.Value
                }));

            return intermediaryCallbackState.Id.ToString();
        }

        /// <summary>
        /// A third-party web API will send a callback request to this endpoint to indicate that the processing of information has started.
        /// </summary>
        /// <param name="text">This is a status from a third-party web API.</param>
        /// <returns>This method returns a task.</returns>
        public async Task ReceiveInitialRequestFromThirdPartyServiceAsync(string text)
        {
            _logger.LogInformation(_communicationStringsProvider.IntermediaryApiCallbackReceivingHttpPostInformation.Value);

            if (text == null)
                throw new ArgumentNullException(nameof(text));

            if (!text.Equals(ProcessStatuses.ThirdPartyServiceStatusStarted, StringComparison.OrdinalIgnoreCase))
                throw ExceptionUtility.GetLoggedWebApiException(_logger, new WebApiException(_communicationStringsProvider.GeneralWebApiExpectedIssueError.Value, _communicationStringsProvider.IntermediaryApiCallbackUnexpectedError.Value, 400, new List<string>
                {
                    $"The status should be {ProcessStatuses.ThirdPartyServiceStatusStarted}."
                }));

            uint numberOfUpdates;

            numberOfUpdates = 0;

            await _intermediaryCallbackStateContext.CallbackStates.ForEachAsync(intermediaryCallbackState =>
            {
                if (intermediaryCallbackState.CreationTimeStamp != default && intermediaryCallbackState.Status == null)
                {
                    if (!IsWithinTimeFrame(intermediaryCallbackState.CreationTimeStamp))
                        throw GetLoggedTimeFrameException();
                    else
                    {
                        numberOfUpdates++;

                        _logger.LogInformation($"Changing the status of the record identified by id={intermediaryCallbackState.Id} to {ProcessStatuses.ThirdPartyServiceStatusStarted}...");

                        intermediaryCallbackState.Status = ProcessStatuses.ThirdPartyServiceStatusStarted;
                        intermediaryCallbackState.LastUpdatedTimeStamp = DateTime.UtcNow;
                    }
                }
            }).ConfigureAwait(false);

            await UpdateAsync(numberOfUpdates).ConfigureAwait(false);
        }

        /// <summary>
        /// A third-party web API will send a callback request to this endpoint with a status about the processing of information.
        /// </summary>
        /// <param name="intermediaryCallbackState">This payload contains information for updating a status record.</param>
        /// <returns>This method returns a task.</returns>
        public async Task UpdateStateAsync(IntermediaryCallbackState intermediaryCallbackState)
        {
            _logger.LogInformation(_communicationStringsProvider.IntermediaryApiCallbackReceivingHttpPutInformation.Value);

            if (intermediaryCallbackState == null)
                throw new ArgumentNullException(nameof(intermediaryCallbackState));

            _logger.LogInformation(_communicationStringsProvider.IntermediaryApiBeforeIterativeUpdateInformation.Value);

            uint numberOfUpdates;

            numberOfUpdates = 0;

            await _intermediaryCallbackStateContext.CallbackStates.ForEachAsync(intermediaryCallbackStateFromStore =>
            {
                if (intermediaryCallbackState.Status != null && intermediaryCallbackState.Status == ProcessStatuses.ThirdPartyServiceStatusStarted || intermediaryCallbackStateFromStore.Status != ProcessStatuses.ThirdPartyServiceStatusCompleted)
                    switch (intermediaryCallbackState.Status)
                    {
                        case ProcessStatuses.ThirdPartyServiceStatusCompleted:
                        case ProcessStatuses.ThirdPartyServiceStatusError:
                        case ProcessStatuses.ThirdPartyServiceStatusProcessed:
                            {
                                if (intermediaryCallbackStateFromStore.LastUpdatedTimeStamp == default || IsWithinTimeFrame(intermediaryCallbackStateFromStore.LastUpdatedTimeStamp))
                                {
                                    numberOfUpdates++;

                                    _logger.LogInformation($"Changing the status of the record identified by id={intermediaryCallbackStateFromStore.Id} to {intermediaryCallbackState.Status}...");

                                    intermediaryCallbackStateFromStore.Status = intermediaryCallbackState.Status;
                                    intermediaryCallbackStateFromStore.Detail = intermediaryCallbackState.Detail;
                                    intermediaryCallbackStateFromStore.LastUpdatedTimeStamp = DateTime.UtcNow;
                                }
                                else
                                    throw GetLoggedTimeFrameException();

                                break;
                            }
                        default:
                            throw ExceptionUtility.GetLoggedWebApiException(_logger, new WebApiException(_communicationStringsProvider.GeneralWebApiExpectedIssueError.Value, _communicationStringsProvider.IntermediaryApiStatusUpdateError.Value, 400, new List<string>
                            {
                                $"The status should be {ProcessStatuses.ThirdPartyServiceStatusProcessed}, {ProcessStatuses.ThirdPartyServiceStatusCompleted}, or {ProcessStatuses.ThirdPartyServiceStatusError}."
                            }));
                    }
            }).ConfigureAwait(false);

            await UpdateAsync(numberOfUpdates).ConfigureAwait(false);
        }

        private bool IsWithinTimeFrame(DateTime timestamp)
        {
            var difference = Math.Abs((timestamp - DateTime.UtcNow).TotalDays);

            return difference <= _intermediaryOptions.WaitDurationInDays;
        }

        private WebApiException GetLoggedTimeFrameException()
        {
            return ExceptionUtility.GetLoggedWebApiException(_logger, new WebApiException(_communicationStringsProvider.GeneralWebApiExpectedIssueError.Value, _communicationStringsProvider.IntermediaryApiRecordUpdateGeneralError.Value, 400, new List<string>
            {
                _communicationStringsProvider.IntermediaryApiCallbackStatusUpdateAllottedTimeFrameError.Value
            }));
        }

        private async Task SaveAsync(uint expectedResult)
        {
            var result = await _intermediaryCallbackStateContext.SaveChangesAsync().ConfigureAwait(false);

            if (result != expectedResult)
                throw ExceptionUtility.GetLoggedWebApiException(_logger, new WebApiException(_communicationStringsProvider.GeneralWebApiExpectedIssueError.Value, _communicationStringsProvider.IntermediaryApiRecordUpdateGeneralError.Value, 400, new List<string>
                {
                    expectedResult == 1 ? _communicationStringsProvider.IntermediaryApiSingleEntityUpdateValidationError.Value : _communicationStringsProvider.IntermediaryApiMultipleEntitiesUpdateValidationError.Value
                }));

            _logger.LogInformation($"The following number indicates the number of records that were saved during the current operation: {expectedResult}.");
            _logger.LogInformation($"The following number indicates the total number of records that have been saved to the store: {_intermediaryCallbackStateContext.CallbackStates.Count()}.");
        }

        private async Task UpdateAsync(uint expectedResult)
        {
            if (_intermediaryCallbackStateContext.ChangeTracker.Entries().Any(entry => entry.State == EntityState.Modified))
            {
                _logger.LogInformation(_communicationStringsProvider.IntermediaryApiBeforeSaveInformation.Value);

                await SaveAsync(expectedResult).ConfigureAwait(false);
            }
        }
    }
}
