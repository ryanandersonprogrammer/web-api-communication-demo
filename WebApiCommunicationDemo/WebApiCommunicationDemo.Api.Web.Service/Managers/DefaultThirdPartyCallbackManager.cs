﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Third Party Callback Manager Default Implementation
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApiCommunicationDemo.Api.Web.Service.Managers
{
    /// <summary>
    /// This is the default implementation of <see cref="IThirdPartyCallbackManager"/>.
    /// </summary>
    public class DefaultThirdPartyCallbackManager : IThirdPartyCallbackManager
    {
        private IDictionary<string, Dictionary<Uri, string>> CallbackRouteMap { get; } = new Dictionary<string, Dictionary<Uri, string>>();

        /// <summary>
        /// This method can be used to access a callback route based on a key.
        /// </summary>
        /// <param name="key">This is a unique key that was provided during an initial request from an intermediary API.</param>
        /// <returns>A task of a callback route will be returned.</returns>
        public Task<Dictionary<Uri, string>> GetCallbackRouteAsync(string key)
        {
            if (key == null)
                throw new ArgumentNullException(nameof(key));

            return Task.FromResult(CallbackRouteMap.ContainsKey(key) ? CallbackRouteMap[key] : null);
        }

        /// <summary>
        /// This method can be used to access a status based on a key and a callback route.
        /// </summary>
        /// <param name="key">This is a unique key that was provided during an initial request from an intermediary API.</param>
        /// <param name="callbackRoute">This is a route that is used to send callback requests to an intermediary API.</param>
        /// <param name="defaultStatus">This value is used if an entry does not exist.</param>
        /// <returns>A task of a status will be returned.</returns>
        public async Task<string> GetCallbackRouteStatusAsync(string key, Uri callbackRoute, string defaultStatus)
        {
            var callbackRouteStatusMap = await GetEntry(key, callbackRoute, defaultStatus).ConfigureAwait(false);

            return callbackRouteStatusMap.ContainsKey(callbackRoute) ? callbackRouteStatusMap[callbackRoute] : null;
        }

        /// <summary>
        /// This method is used to create dictionaries of callback information.
        /// </summary>
        /// <param name="key">This is a unique key that was provided during an initial request from an intermediary API.</param>
        /// <param name="status">This is a status of work being performed.</param>
        /// <param name="callbackRoute">This is a route that is used to send callback requests to an intermediary API.</param>
        /// <returns>This method returns a task of a dictionary that contains callback information.</returns>
        public async Task<Dictionary<Uri, string>> AddCallbackRouteAsync(string key, string status, Uri callbackRoute)
        {
            return await GetEntry(key, callbackRoute, status).ConfigureAwait(false);
        }

        /// <summary>
        /// This method is used to create dictionaries of callback information.
        /// </summary>
        /// <param name="key">This is a unique key that was provided during an initial request from an intermediary API.</param>
        /// <param name="status">This is a status of work being performed.</param>
        /// <param name="callbackRoute">This is a route that is used to send callback requests to an intermediary API.</param>
        /// <returns>This method returns a task of a status.</returns>
        public async Task<string> AddCallbackRouteAndReturnStatusAsync(string key, string status, Uri callbackRoute)
        {
            var callbackRouteStatusMap = await AddCallbackRouteAsync(key, status, callbackRoute).ConfigureAwait(false);

            return callbackRouteStatusMap[callbackRoute];
        }

        /// <summary>
        /// This method is used to update dictionaries of callback information.
        /// </summary>
        /// <param name="key">This is a unique key that was provided during an initial request from an intermediary API.</param>
        /// <param name="callbackRoute">This is a route that is used to send callback requests to an intermediary API.</param>
        /// <param name="status">This is a status of work being performed.</param>
        /// <returns>This method returns a task of a value that determines whether or not an entry was updated.</returns>
        public Task<bool> UpdateCallbackRouteAsync(string key, Uri callbackRoute, string status)
        {
            if (!CallbackRouteMap[key].ContainsKey(callbackRoute))
                return Task.FromResult(false);

            CallbackRouteMap[key][callbackRoute] = status;

            return Task.FromResult(true);
        }

        private Dictionary<Uri, string> CreateEntry(string key, Uri callbackRoute, string status)
        {
            var callbackRouteStatusMap = new Dictionary<Uri, string>();

            CallbackRouteMap.Add(key, callbackRouteStatusMap);

            CallbackRouteMap[key].Add(callbackRoute, status);

            return callbackRouteStatusMap;
        }

        private async Task<Dictionary<Uri, string>> GetEntry(string key, Uri callbackRoute, string status)
        {
            var callbackRouteStatusMap = await GetCallbackRouteAsync(key).ConfigureAwait(false);

            if (callbackRouteStatusMap == null)
                callbackRouteStatusMap = CreateEntry(key, callbackRoute, status);

            return callbackRouteStatusMap;
        }
    }
}
