﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Third Party Callback Manager Interface
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApiCommunicationDemo.Api.Web.Service.Managers
{
    /// <summary>
    /// This is a contract for a manager that can be used to perform CRUD operations on information related to callback routes.
    /// </summary>
    public interface IThirdPartyCallbackManager
    {
        /// <summary>
        /// This method can be used to access a callback route based on a key.
        /// </summary>
        /// <param name="key">This is a unique key that was provided during an initial request from an intermediary API.</param>
        /// <returns>A task of a callback route will be returned.</returns>
        Task<Dictionary<Uri, string>> GetCallbackRouteAsync(string key);

        /// <summary>
        /// This method can be used to access a status based on a key and a callback route.
        /// </summary>
        /// <param name="key">This is a unique key that was provided during an initial request from an intermediary API.</param>
        /// <param name="callbackRoute">This is a route that is used to send callback requests to an intermediary API.</param>
        /// <param name="defaultStatus">This value is used if an entry does not exist.</param>
        /// <returns>A task of a status will be returned.</returns>
        Task<string> GetCallbackRouteStatusAsync(string key, Uri callbackRoute, string defaultStatus);

        /// <summary>
        /// This method is used to create dictionaries of callback information.
        /// </summary>
        /// <param name="key">This is a unique key that was provided during an initial request from an intermediary API.</param>
        /// <param name="status">This is a status of work being performed.</param>
        /// <param name="callbackRoute">This is a route that is used to send callback requests to an intermediary API.</param>
        /// <returns>This method returns a task of a dictionary that contains callback information.</returns>
        Task<Dictionary<Uri, string>> AddCallbackRouteAsync(string key, string status, Uri callbackRoute);

        /// <summary>
        /// This method is used to create dictionaries of callback information.
        /// </summary>
        /// <param name="key">This is a unique key that was provided during an initial request from an intermediary API.</param>
        /// <param name="status">This is a status of work being performed.</param>
        /// <param name="callbackRoute">This is a route that is used to send callback requests to an intermediary API.</param>
        /// <returns>This method returns a task of a status.</returns>
        Task<string> AddCallbackRouteAndReturnStatusAsync(string key, string status, Uri callbackRoute);

        /// <summary>
        /// This method is used to update dictionaries of callback information.
        /// </summary>
        /// <param name="key">This is a unique key that was provided during an initial request from an intermediary API.</param>
        /// <param name="callbackRoute">This is a route that is used to send callback requests to an intermediary API.</param>
        /// <param name="status">This is a status of work being performed.</param>
        /// <returns>This method returns a task of a value that determines whether or not an entry was updated.</returns>
        Task<bool> UpdateCallbackRouteAsync(string key, Uri callbackRoute, string status);
    }
}
