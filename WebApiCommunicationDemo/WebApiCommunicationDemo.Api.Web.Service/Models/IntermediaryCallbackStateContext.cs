﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Intermediary Callback State Database Context
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.EntityFrameworkCore;

namespace WebApiCommunicationDemo.Api.Web.Service.Models
{
    /// <summary>
    /// This is a database context for statuses that are maintained whenever communication with a third-party web API takes place.
    /// </summary>
    public class IntermediaryCallbackStateContext : DbContext
    {
        /// <summary>
        /// This constructor can be used to initialize a <see cref="IntermediaryCallbackStateContext"/>.
        /// </summary>
        /// <param name="options">These are options that will be used by a database context that is based on an <see cref="IntermediaryCallbackStateContext"/>.</param>
        public IntermediaryCallbackStateContext(DbContextOptions<IntermediaryCallbackStateContext> options) : base(options)
        {
        }

        /// <summary>
        /// This is a collection of statuses that are stored in a database.
        /// </summary>
        public DbSet<IntermediaryCallbackState> CallbackStates { get; set; }
    }
}
