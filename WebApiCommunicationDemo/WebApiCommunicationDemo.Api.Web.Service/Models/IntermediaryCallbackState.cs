﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Intermediary Callback State Model
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System;
using WebApiCommunicationDemo.Api.Web.Common.Models;

namespace WebApiCommunicationDemo.Api.Web.Service.Models
{
    /// <summary>
    /// This entity is used to keep track of work being performed by a third-party web API.
    /// </summary>
    public class IntermediaryCallbackState : BaseEntity<Guid>
    {
        /// <summary>
        /// This is an expected status of work that is being performed by a third-party web API.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// This is information about work that is being performed by a third-party web API.
        /// </summary>
        public string Detail { get; set; }

        /// <summary>
        /// This is a body of information that will be processed by a third-party web API.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// This property stores the date and time when an instance of this class was created.
        /// </summary>
        public DateTime CreationTimeStamp { get; set; }

        /// <summary>
        /// This property stores the date and time when an instance of this class was updated.
        /// </summary>
        public DateTime LastUpdatedTimeStamp { get; set; }
    }
}
