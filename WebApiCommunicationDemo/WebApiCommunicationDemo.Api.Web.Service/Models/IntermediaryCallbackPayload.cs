﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Intermediary Callback Payload Model
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System.ComponentModel.DataAnnotations;

namespace WebApiCommunicationDemo.Api.Web.Service.Models
{
    /// <summary>
    /// This payload is used to provide a third-party web API with a callback route and information to process.
    /// </summary>
    public class IntermediaryCallbackPayload
    {
        /// <summary>
        /// This is information that will be processed by a third-party web API.
        /// </summary>
        [Required]
        public string Body { get; set; }

        /// <summary>
        /// A third-party web API can send a callback request to this route when processing is finished.
        /// </summary>
        [Required]
        public string Callback { get; set; }
    }
}
