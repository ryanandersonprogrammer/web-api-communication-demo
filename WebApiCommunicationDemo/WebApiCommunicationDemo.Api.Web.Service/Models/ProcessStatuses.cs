﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Process Status Constants
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
namespace WebApiCommunicationDemo.Api.Web.Service.Models
{
    /// <summary>
    /// This class contains constant values for statuses that are sent by a third-party web API while work is being performed.
    /// </summary>
    public static class ProcessStatuses
    {
        /// <summary>
        /// This constant is used to indicate that processing is in a stopped state.
        /// </summary>
        public const string ThirdPartyServiceStatusStopped = "STOPPED";

        /// <summary>
        /// This constant is used to indicate that a process has started.
        /// </summary>
        public const string ThirdPartyServiceStatusStarted = "STARTED";

        /// <summary>
        /// This constant is used to indicate that information has been processed.
        /// </summary>
        public const string ThirdPartyServiceStatusProcessed = "PROCESSED";

        /// <summary>
        /// This constant is used to indicate that processing has completed.
        /// </summary>
        public const string ThirdPartyServiceStatusCompleted = "COMPLETED";

        /// <summary>
        /// This constant is used to indicate that an error has occurred during the processing of information.
        /// </summary>
        public const string ThirdPartyServiceStatusError = "ERROR";
    }
}
