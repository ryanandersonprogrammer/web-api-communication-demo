﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Third Party Service Default Implementation
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Common.Exceptions;
using WebApiCommunicationDemo.Api.Web.Common.Exceptions.Utility;
using WebApiCommunicationDemo.Api.Web.Service.Clients;
using WebApiCommunicationDemo.Api.Web.Service.Managers;
using WebApiCommunicationDemo.Api.Web.Service.Models;
using WebApiCommunicationDemo.Api.Web.Service.Options;
using WebApiCommunicationDemo.Api.Web.Service.Providers;

namespace WebApiCommunicationDemo.Api.Web.Service
{
    /// <summary>
    /// This is the default implementation of <see cref="IThirdPartyService"/>.
    /// </summary>
    public class DefaultThirdPartyService : IThirdPartyService
    {
        private readonly IIntermediaryClient _intermediaryClient;

        private readonly IThirdPartyCallbackManager _thirdPartyCallbackManager;

        private readonly ICommunicationStringsProvider _communicationStringsProvider;

        private readonly ILogger<DefaultThirdPartyService> _logger;

        private readonly ThirdPartyOptions _thirdPartyOptions;

        /// <summary>
        /// This constructor can be used to initialize a <see cref="DefaultThirdPartyService" />.
        /// </summary>
        /// <param name="logger">This is a logger that is used to record events.</param>
        /// <param name="communicationStringsProvider">This is the <see cref="ICommunicationStringsProvider"/> that provides strings for logging.</param>
        /// <param name="intermediaryClient">This is the <see cref="IIntermediaryClient"/> that is used to communicate with an intermediary API.</param>
        /// <param name="thirdPartyCallbackManager">This is the <see cref="IThirdPartyCallbackManager"/> that is used to manage information about callback routes.</param>
        /// <param name="thirdPartyOptions">These are the <see cref="ThirdPartyOptions"/> that are used to configure a service for communication with an intermediary API.</param>
        public DefaultThirdPartyService(ILogger<DefaultThirdPartyService> logger, ICommunicationStringsProvider communicationStringsProvider, IIntermediaryClient intermediaryClient, IThirdPartyCallbackManager thirdPartyCallbackManager, IOptions<ThirdPartyOptions> thirdPartyOptions)
        {
            if (thirdPartyOptions == null)
                throw new ArgumentNullException(nameof(thirdPartyOptions));

            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _communicationStringsProvider = communicationStringsProvider ?? throw new ArgumentNullException(nameof(communicationStringsProvider));
            _intermediaryClient = intermediaryClient ?? throw new ArgumentNullException(nameof(intermediaryClient));
            _thirdPartyCallbackManager = thirdPartyCallbackManager ?? throw new ArgumentNullException(nameof(thirdPartyCallbackManager));
            _thirdPartyOptions = thirdPartyOptions.Value;
        }

        /// <summary>
        /// This method is used to start processing information and relaying statuses to an intermediary web API through a provided callback route.
        /// </summary>
        /// <param name="intermediaryCallbackPayload">This payload contains a callback route and information to be processed by a third-party web API.</param>
        /// <returns>This method returns a task.</returns>
        public Task StartAsync(IntermediaryCallbackPayload intermediaryCallbackPayload)
        {
            if (intermediaryCallbackPayload == null)
                throw new ArgumentNullException(nameof(intermediaryCallbackPayload));

            var bodySubstring = intermediaryCallbackPayload.Body.Substring(0, Math.Min(intermediaryCallbackPayload.Body.Length, 200));

            _logger.LogInformation(_communicationStringsProvider.ThirdPartyApiHttpPostReceivingRequestInformation.Value);
            _logger.LogInformation($"The following text was sent in the body of the request: {bodySubstring}...");

            if (string.IsNullOrWhiteSpace(intermediaryCallbackPayload.Body))
                ExceptionUtility.GetLoggedWebApiException(_logger, new WebApiException(_communicationStringsProvider.GeneralWebApiExpectedIssueError.Value, _communicationStringsProvider.ThirdPartyApiCallbackCannotStartProcessingError.Value, 400, new List<string>
                {
                    _communicationStringsProvider.ThirdPartyApiCallbackInvalidBodyError.Value
                }));

            var processingTask = Task.Run(async () =>
            {
                var callbackRouteUri = new Uri(_thirdPartyOptions.IntermediaryBaseRequestUri.OriginalString + intermediaryCallbackPayload.Callback, UriKind.Relative);

                var currentStatus = await _thirdPartyCallbackManager.GetCallbackRouteStatusAsync(intermediaryCallbackPayload.Body, callbackRouteUri, ProcessStatuses.ThirdPartyServiceStatusStopped).ConfigureAwait(false);

                if (currentStatus == ProcessStatuses.ThirdPartyServiceStatusStopped)
                {
                    await Task.Delay(_thirdPartyOptions.AmountOfTimeBetweenCallbackRequests).ConfigureAwait(false);

                    _logger.LogInformation($"Attempting to start processing information for {bodySubstring} at {callbackRouteUri.OriginalString}...");

                    currentStatus = ProcessStatuses.ThirdPartyServiceStatusStarted;

                    var response = await _intermediaryClient.StartAsync(currentStatus, callbackRouteUri).ConfigureAwait(false);

                    ValidateResponse(response);

                    await ChangeStatusAsync(intermediaryCallbackPayload, bodySubstring, callbackRouteUri, currentStatus).ConfigureAwait(false);
                }

                for (int i = 0; i < _thirdPartyOptions.NumberOfCallbackRequests && currentStatus != ProcessStatuses.ThirdPartyServiceStatusCompleted; i++)
                {
                    await Task.Delay(_thirdPartyOptions.AmountOfTimeBetweenCallbackRequests).ConfigureAwait(false);

                    var statuses = new string[]
                    {
                        ProcessStatuses.ThirdPartyServiceStatusCompleted,
                        ProcessStatuses.ThirdPartyServiceStatusError,
                        ProcessStatuses.ThirdPartyServiceStatusProcessed
                    };

                    var randomIndex = new Random().Next(0, 3);

                    currentStatus = statuses[randomIndex];

                    _logger.LogInformation($"The status for {bodySubstring} at {callbackRouteUri.OriginalString} is {currentStatus}...");
                    _logger.LogInformation($"The current processing index for {bodySubstring} at {callbackRouteUri.OriginalString} is {i}...");

                    var intermediaryCallbackState = new IntermediaryCallbackState { Status = currentStatus, Detail = $"The record identified by {intermediaryCallbackPayload.Body} was updated." };

                    var response = await _intermediaryClient.ProcessAsync(intermediaryCallbackState, callbackRouteUri).ConfigureAwait(false);

                    ValidateResponse(response);

                    await ChangeStatusAsync(intermediaryCallbackPayload, bodySubstring, callbackRouteUri, currentStatus).ConfigureAwait(false);
                }

                if (_thirdPartyOptions.ContinueUpdatingAfterAttemptingToComplete)
                {
                    currentStatus = ProcessStatuses.ThirdPartyServiceStatusStopped;

                    _logger.LogInformation($"Attempting to reset the status for {bodySubstring} at {callbackRouteUri.OriginalString} to {currentStatus}...");

                    var updateResult = await UpdateCallbackRouteAsync(intermediaryCallbackPayload, bodySubstring, callbackRouteUri, currentStatus).ConfigureAwait(false);

                    if (!updateResult)
                        _logger.LogInformation("The status could not be reset...");

                    throw new Exception("End task after attempting to reset a status...");
                }
            }).ContinueWith(task =>
            {
                if (task.IsFaulted)
                    _logger.LogInformation($"The previous task completed with the following message: \"{task.Exception.InnerException.Message}\"...");
            }, TaskScheduler.Default);
            
            _logger.LogInformation($"The current status of the background task that is identified by {processingTask.Id} is {processingTask.Status}...");

            return Task.CompletedTask;
        }

        private void ValidateResponse(HttpResponseMessage response)
        {
            if (response.StatusCode != HttpStatusCode.NoContent)
                throw ExceptionUtility.GetLoggedWebApiException(_logger, new WebApiException(_communicationStringsProvider.GeneralWebApiExpectedIssueError.Value, "The intermediary web API did not respond with 204 after a callback request was sent.", 400, new List<string>
                {
                    "An error was encountered while trying to send callback requests to an intermediary web API."
                }));
        }

        private async Task<bool> UpdateCallbackRouteAsync(IntermediaryCallbackPayload intermediaryCallbackPayload, string bodySubstring, Uri callbackRouteUri, string currentStatus)
        {
            var entryWasUpdated = await _thirdPartyCallbackManager.UpdateCallbackRouteAsync(intermediaryCallbackPayload.Body, callbackRouteUri, currentStatus).ConfigureAwait(false);

            if (!entryWasUpdated)
            {
                _logger.LogInformation($"The status for {bodySubstring} at {callbackRouteUri.OriginalString} could not be changed to {currentStatus}...");

                return false;
            }

            return true;
        }

        private async Task ChangeStatusAsync(IntermediaryCallbackPayload intermediaryCallbackPayload, string bodySubstring, Uri callbackRouteUri, string currentStatus)
        {
            _logger.LogInformation($"Attempting to change the status for {bodySubstring} at {callbackRouteUri.OriginalString} to {currentStatus}...");

            var updateResult = await UpdateCallbackRouteAsync(intermediaryCallbackPayload, bodySubstring, callbackRouteUri, currentStatus).ConfigureAwait(false);

            if (!updateResult)
            {
                _logger.LogInformation($"The status for {bodySubstring} could not be changed...");

                throw new Exception($"End task after attempting to change a status for {bodySubstring} ...");
            }
        }
    }
}