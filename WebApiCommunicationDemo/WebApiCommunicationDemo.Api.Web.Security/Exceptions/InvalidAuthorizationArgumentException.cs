﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Invalid Authorization Argument Exception
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System;

namespace WebApiCommunicationDemo.Api.Web.Security.Exceptions
{
    /// <summary>
    /// This exception can be used when an argument for creating an authorization parameter is invalid.
    /// </summary>
    public class InvalidAuthorizationArgumentException : Exception
    {
        /// <summary>
        /// This constructor can be used to initialize a <see cref="InvalidAuthorizationArgumentException"/>.
        /// </summary>
        public InvalidAuthorizationArgumentException()
        {
        }

        /// <summary>
        /// This constructor can be used to initialize a <see cref="InvalidAuthorizationArgumentException"/>.
        /// </summary>
        /// <param name="message">This is a message that describes the exception.</param>
        public InvalidAuthorizationArgumentException(string message) : base(message)
        {
        }

        /// <summary>
        /// This constructor can be used to initialize a <see cref="InvalidAuthorizationArgumentException"/>.
        /// </summary>
        /// <param name="message">This is a message that describes the exception.</param>
        /// <param name="innerException">This is an exception that caused a <see cref="InvalidAuthorizationArgumentException"/> to be thrown.</param>
        public InvalidAuthorizationArgumentException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
