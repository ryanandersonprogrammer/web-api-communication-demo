﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Authorize with Basic HTTP Authentication Attribute
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using System;
using WebApiCommunicationDemo.Api.Web.Security.Models;

namespace WebApiCommunicationDemo.Api.Web.Security.Attributes
{
    /// <summary>
    /// This attribute can be used to filter authorization requests that use "Basic" HTTP authentication.
    /// </summary>
    public sealed class AuthorizeWithBasicHttpAuthenticationAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// This is used to create a challenge for "Basic" HTTP Authentication.
        /// </summary>
        public BasicHttpAuthenticationResponse BasicHttpAuthenticationResponse { get; set; }

        /// <summary>
        /// This determines whether or not authorization should be enabled.
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// This constructor can be used to initialize a <see cref="AuthorizeWithBasicHttpAuthenticationAttribute"./>
        /// </summary>
        /// <param name="credentialIdentifier">This is used to uniquely identify a client.</param>
        /// <param name="credentialSecret">This is a secret that is shared between a server and a client.</param>
        /// <param name="realm">This is a description of the protected resource.</param>
        /// <param name="isEnabled">This determines whether or not authorization should be enabled.</param>
        public AuthorizeWithBasicHttpAuthenticationAttribute(string credentialIdentifier, string credentialSecret, string realm, bool isEnabled = true)
        {
            BasicHttpAuthenticationResponse = new BasicHttpAuthenticationResponse(new BasicHttpAuthentication(credentialIdentifier, credentialSecret), realm);

            IsEnabled = isEnabled;
        }

        /// <summary>
        /// This method handles the authorization flow when an action is executing. 
        /// </summary>
        /// <param name="context">This is the context of an action filter.</param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (IsEnabled)
            {
                if (context == null)
                    throw new ArgumentNullException(nameof(context));

                var request = context.HttpContext.Request;

                if (request.Headers.TryGetValue("Authorization", out StringValues value))
                {
                    if (!BasicHttpAuthenticationResponse.IsValidAndAuthorized(value))
                    {
                        context.HttpContext.Response.Headers.Add("WWW-Authenticate", BasicHttpAuthenticationResponse.GetParameter());
                        context.Result = new UnauthorizedResult();
                    }
                }
                else
                    context.Result = new ForbidResult();
            }

            base.OnActionExecuting(context);
        }
    }
}
