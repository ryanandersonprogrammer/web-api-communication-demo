﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Authorization Base Model
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
namespace WebApiCommunicationDemo.Api.Web.Security.Models
{
    /// <summary>
    /// This is the base model of a scheme that is used in requests and responses for authorization.
    /// </summary>
    public abstract class AuthorizationModelBase
    {
        /// <summary>
        /// This is the name of the scheme that will be sent in the header of either a request or a response.
        /// </summary>
        public abstract string Scheme { get; }

        /// <summary>
        /// This is the value of an authorization header.
        /// </summary>
        public string HeaderValue => GetAuthorizationHeaderValue(GetParameter());

        /// <summary>
        /// This method formats the name of a scheme and a parameter into a string that can be used in an authorization request.
        /// </summary>
        /// <param name="parameter">This is an authorization parameter.</param>
        /// <returns>This method will return a string that can be used as the value of an authorization header.</returns>
        protected string GetAuthorizationHeaderValue(string parameter)
        {
            return $"{Scheme} {parameter}";
        }

        /// <summary>
        /// This method can be used to retrieve the parameter that is associated with a scheme.
        /// </summary>
        /// <returns>This method returns a string parameter that is associated with a scheme.</returns>
        public abstract string GetParameter();
    }
}