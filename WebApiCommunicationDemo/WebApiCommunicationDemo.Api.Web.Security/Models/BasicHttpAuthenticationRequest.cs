﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Basic HTTP Authentication Request Model
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System;
using System.Text;

namespace WebApiCommunicationDemo.Api.Web.Security.Models
{
    /// <summary>
    /// This class can be used to create an authorization request for "Basic" HTTP Authentication.
    /// </summary>
    public sealed class BasicHttpAuthenticationRequest : AuthorizationModelBase
    {
        /// <summary>
        /// This constructor can be used to initialize a <see cref="BasicHttpAuthenticationRequest"/>.
        /// </summary>
        /// <param name="basicHttpAuthentication">This is used to specify arguments for a parameter that conforms to "Basic" HTTP Authentication.</param>
        public BasicHttpAuthenticationRequest(BasicHttpAuthentication basicHttpAuthentication)
        {
            BasicHttpAuthentication = basicHttpAuthentication;
        }

        /// <summary>
        /// This is the name of the scheme that will be sent in the header of either a request or a response.
        /// </summary>
        public override string Scheme
        {
            get
            {
                return AuthorizationSchemes.BasicHttpAuthentication;
            }
        }

        /// <summary>
        /// This is used to specify arguments for a parameter that conforms to "Basic" HTTP Authentication.
        /// </summary>
        public BasicHttpAuthentication BasicHttpAuthentication { get; }

        /// <summary>
        /// This method can be used to retrieve a parameter for an authorization request.
        /// </summary>
        /// <returns>A parameter for the HTTP Authorization header will be returned.</returns>
        public override string GetParameter()
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes($"{BasicHttpAuthentication.ClientIdentifier}:{BasicHttpAuthentication.ClientSecret}"));
        }
    }
}