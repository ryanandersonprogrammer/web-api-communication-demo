﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Basic HTTP Authentication Model
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using WebApiCommunicationDemo.Api.Web.Security.Exceptions;

namespace WebApiCommunicationDemo.Api.Web.Security.Models
{
    /// <summary>
    /// This is used to specify arguments for a parameter that conforms to "Basic" HTTP Authentication.
    /// </summary>
    public sealed class BasicHttpAuthentication
    {
        private string _clientIdentifier;

        /// <summary>
        /// This constructor can be used to initialize a <see cref="BasicHttpAuthentication"/>.
        /// </summary>
        /// <param name="clientIdentifier">This is a string that identifies a client.</param>
        /// <param name="clientSecret">This is a secret that is shared between a client and a server.</param>
        public BasicHttpAuthentication(string clientIdentifier, string clientSecret)
        {
            ClientIdentifier = clientIdentifier;
            ClientSecret = clientSecret;
        }

        /// <summary>
        /// This is a string that identifies a client.
        /// </summary>
        public string ClientIdentifier
        {
            get
            {
                return _clientIdentifier;
            }
            set
            {
                if (value != null && value.Contains(':', System.StringComparison.InvariantCulture))
                    throw new InvalidAuthorizationArgumentException($"The format of the \"{AuthorizationSchemes.BasicHttpAuthentication}\" HTTP Authentication header is invalid: {value}.");

                _clientIdentifier = value;
            }
        }

        /// <summary>
        /// This is a secret that is shared between a client and a server.
        /// </summary>
        public string ClientSecret { get; set; }
    }
}
