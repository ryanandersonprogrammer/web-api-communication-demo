﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Authorization Response Base Model
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
namespace WebApiCommunicationDemo.Api.Web.Security.Models
{
    /// <summary>
    /// This is the base model of a scheme that can be used in an authorization response.
    /// </summary>
    public abstract class AuthorizationResponseModelBase : AuthorizationModelBase
    {
        /// <summary>
        /// This is a value that can be used once in an authorization process.
        /// </summary>
        public string Nonce { get; set; }

        /// <summary>
        /// This is the realm for a challenge.
        /// </summary>
        public string Realm { get; set; }

        /// <summary>
        /// This method can be used to determine whether the parameter from the value of an authorization header is valid according to 
        /// the specification of "Basic" HTTP Authentication.
        /// </summary>
        /// <param name="parameter">This is a parameter from an authorization header.</param>
        /// <returns>This method will return true if a value is valid; otherwise, false will be returned.</returns>
        protected abstract bool CanAuthorize(string parameter);

        /// <summary>
        /// This method can be used to determine whether an authorization value is valid according to a certain specification.
        /// </summary>
        /// <param name="headerValue">This is a value from an authorization header.</param>
        /// <returns>This method will return true if the header value and credentials are valid; otherwise, it will return false.</returns>
        public bool IsValidAndAuthorized(string headerValue)
        {
            return TryGetParameterFromHeaderValue(headerValue, out string parameter) ? CanAuthorize(parameter) : false;
        }

        /// <summary>
        /// This method can be used to try extracting a parameter from the value of an authorization header.
        /// </summary>
        /// <param name="headerValue">This is a value from an authorization header.</param>
        /// <param name="parameter">This is the parameter that was extracted from the header value.</param>
        /// <returns>This method will return false if a parameter cannot be extracted; otherwise, it will return true.</returns>
        public bool TryGetParameterFromHeaderValue(string headerValue, out string parameter)
        {
            parameter = null;

            if (Scheme.Length < headerValue?.Length)
            {
                var lengthOffset = Scheme.Length + 1;

                var valueSubstring = headerValue.Substring(0, lengthOffset);

                if (valueSubstring == $"{Scheme} ")
                {
                    parameter = headerValue.Substring(lengthOffset).TrimStart();

                    return true;
                }
            }

            return false;
        }
    }
}