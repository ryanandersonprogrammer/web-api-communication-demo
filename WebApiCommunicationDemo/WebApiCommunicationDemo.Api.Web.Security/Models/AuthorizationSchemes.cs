﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Authorization Scheme Constants
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
namespace WebApiCommunicationDemo.Api.Web.Security.Models
{
    /// <summary>
    /// This class contains constants for authorization schemes.
    /// </summary>
    public static class AuthorizationSchemes
    {
        /// <summary>
        /// This constant can be used to specify that "Basic" HTTP Authentication is being used in an authorization process.
        /// </summary>
        public const string BasicHttpAuthentication = "Basic";
    }
}
