﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Basic HTTP Authentication Response Model
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using System;
using System.Text;

namespace WebApiCommunicationDemo.Api.Web.Security.Models
{
    /// <summary>
    /// This class can be used to create a challenge response for "Basic" HTTP Authentication.
    /// </summary>
    public sealed class BasicHttpAuthenticationResponse : AuthorizationResponseModelBase
    {
        /// <summary>
        /// This constructor can be used to initialize a <see cref="BasicHttpAuthenticationResponse"/>.
        /// </summary>
        /// <param name="basicHttpAuthentication">This is used to specify arguments for a parameter that conforms to "Basic" HTTP Authentication.</param>
        /// <param name="realm">This is a realm for a challenge.</param>
        public BasicHttpAuthenticationResponse(BasicHttpAuthentication basicHttpAuthentication, string realm)
        {
            BasicHttpAuthentication = basicHttpAuthentication;
            Realm = realm;
        }

        /// <summary>
        /// This is the name of the scheme that will be sent in the header of either a request or a response.
        /// </summary>
        public override string Scheme
        {
            get
            {
                return AuthorizationSchemes.BasicHttpAuthentication;
            }
        }

        /// <summary>
        /// This is used to specify arguments for a parameter that conforms to "Basic" HTTP Authentication.
        /// </summary>
        public BasicHttpAuthentication BasicHttpAuthentication { get; }

        /// <summary>
        /// This method can be used to retrieve a parameter for a challenge.
        /// </summary>
        /// <returns>A parameter for the HTTP WWW-Authenticate header will be returned.</returns>
        public override string GetParameter()
        {
            return $"realm=\"{Realm}\"";
        }

        /// <summary>
        /// This method can be used to determine whether the parameter from the value of an authorization header is valid according to 
        /// the specification of "Basic" HTTP Authentication.
        /// </summary>
        /// <param name="parameter">This is a parameter from an authorization header.</param>
        /// <returns>This method will return true if a value is valid; otherwise, false will be returned.</returns>
        protected override bool CanAuthorize(string parameter)
        {
            var parameterBytes = Convert.FromBase64String(parameter);

            var decodedValue = Encoding.UTF8.GetString(parameterBytes);

            var indexOfFirstColon = decodedValue.IndexOf(':', StringComparison.InvariantCulture);

            var credentialIdentifier = decodedValue.Substring(0, indexOfFirstColon);
            var credentialSecret = decodedValue.Substring(indexOfFirstColon + 1);

            return credentialIdentifier == BasicHttpAuthentication.ClientIdentifier && credentialSecret == BasicHttpAuthentication.ClientSecret;
        }
    }
}