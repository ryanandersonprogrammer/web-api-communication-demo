/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Startup
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using WebApiCommunicationDemo.Api.Web.Security.Models;
using WebApiCommunicationDemoExtensions.DependencyInjection;

namespace WebApiCommunicationDemo.Api.Web.ThirdParty
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddThirdParty(thirdPartyOptions =>
            {
                var intermediaryBaseAddress = Configuration[nameof(thirdPartyOptions.IntermediaryBaseAddress)];
                var intermediaryBaseUri = Configuration[nameof(thirdPartyOptions.IntermediaryBaseRequestUri)];

                thirdPartyOptions.IntermediaryBaseAddress = new Uri(!string.IsNullOrWhiteSpace(intermediaryBaseAddress) ? intermediaryBaseAddress : "https://localhost:44379/");
                thirdPartyOptions.IntermediaryBaseRequestUri = new Uri(!string.IsNullOrWhiteSpace(intermediaryBaseUri) ? intermediaryBaseUri : "intermediary/v0.1/communication", UriKind.Relative);
                thirdPartyOptions.CallbackResourceAuthentication = new BasicHttpAuthenticationRequest(new BasicHttpAuthentication("client-identifier", "client-secret"));
                thirdPartyOptions.StartProcessingResourceAuthentication = new BasicHttpAuthenticationResponse(new BasicHttpAuthentication("client-identifier", "client-secret"), "Third Party API Stop Processing Resource");
                thirdPartyOptions.NumberOfCallbackRequests = int.TryParse(Configuration[nameof(thirdPartyOptions.NumberOfCallbackRequests)], out int numberOfCallbackRequests) ? numberOfCallbackRequests : 10;
                thirdPartyOptions.AmountOfTimeBetweenCallbackRequests = TimeSpan.FromSeconds(double.TryParse(Configuration[$"{nameof(thirdPartyOptions.AmountOfTimeBetweenCallbackRequests)}InSeconds"], out double amountOfTimeBetweenCallbackRequests) ? amountOfTimeBetweenCallbackRequests : 10);
                thirdPartyOptions.ContinueUpdatingAfterAttemptingToComplete = bool.TryParse(Configuration[nameof(thirdPartyOptions.ContinueUpdatingAfterAttemptingToComplete)], out bool continueUpdatingAfterCompletion) ? continueUpdatingAfterCompletion : true;
            });

            services.AddControllers();

            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(0, 1);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public static void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
