﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Third Party Work API
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Common.Attributes;
using WebApiCommunicationDemo.Api.Web.Security.Attributes;
using WebApiCommunicationDemo.Api.Web.Service;
using WebApiCommunicationDemo.Api.Web.Service.Models;

namespace WebApiCommunicationDemo.Api.Web.ThirdParty.Controllers
{
    /// <summary>
    /// This web API contains endpoints for communicating with an intermediary web API to process information and report statuses.
    /// </summary>
    [ApiController]
    [ApiVersion("0.1")]
    [Route("thirdparty/v{v:apiVersion}/[controller]")]
    [WebApiExceptionFilter]
    public class WorkController : ControllerBase
    {
        private readonly IThirdPartyService _thirdPartyService;

        /// <summary>
        /// This constructor can be used to initialize a <see cref="WorkController"/>.
        /// </summary>
        /// <param name="thirdPartyService">This is a controller that contains an endpoint that can be used by an intermediary web API to process information and receive statuses.</param>
        public WorkController(IThirdPartyService thirdPartyService)
        {
            _thirdPartyService = thirdPartyService ?? throw new ArgumentNullException(nameof(thirdPartyService));
        }

        /// <summary>
        /// This endpoint can be used to start the processing of information.
        /// </summary>
        /// <remarks>
        /// This endpoint will communicate with an intermediary web API to report statuses while information is being processed.
        /// </remarks>
        /// <param name="intermediaryCallbackPayload">This payload is used to provide a third-party web API with a callback route and information to process.</param>
        /// <returns>This method returns a task.</returns>
        [HttpPost("process")]
        [ValidateModel]
        [AuthorizeWithBasicHttpAuthentication("client-identifier", "client-secret", "Third Party API Stop Processing Resource")]
        public async Task<IActionResult> ReceiveInitialRequestAsync([FromBody][Required] IntermediaryCallbackPayload intermediaryCallbackPayload)
        {
            await _thirdPartyService.StartAsync(intermediaryCallbackPayload).ConfigureAwait(false);

            return NoContent();
        }
    }
}
