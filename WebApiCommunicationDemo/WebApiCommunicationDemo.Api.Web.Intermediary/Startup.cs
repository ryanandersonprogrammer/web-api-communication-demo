/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Startup
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using WebApiCommunicationDemo.Api.Web.Security.Models;
using WebApiCommunicationDemo.Extensions.DependencyInjection;

namespace WebApiCommunicationDemo.Api.Web.Intermediary
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIntermediary(intermediaryOptions =>
            {
                var thirdPartyStubBaseAddress = Configuration[nameof(intermediaryOptions.ThirdPartyStubBaseAddress)];
                var thirdPartyStubBaseRequestUri = Configuration[nameof(intermediaryOptions.ThirdPartyStubBaseRequestUri)];
                var thirdPartyStubRequestUri = Configuration[nameof(intermediaryOptions.ThirdPartyStubRequestUri)];

                intermediaryOptions.ThirdPartyStubBaseAddress = new Uri(!string.IsNullOrWhiteSpace(thirdPartyStubBaseAddress) ? thirdPartyStubBaseAddress : "https://localhost:44365/");
                intermediaryOptions.ThirdPartyStubBaseRequestUri = new Uri(!string.IsNullOrWhiteSpace(thirdPartyStubBaseRequestUri) ? thirdPartyStubBaseRequestUri : "thirdparty/v0.1/work", UriKind.Relative);
                intermediaryOptions.ThirdPartyStubRequestUri = new Uri(!string.IsNullOrWhiteSpace(thirdPartyStubRequestUri) ? thirdPartyStubRequestUri : "process", UriKind.Relative);
                intermediaryOptions.CallbackRoute = Configuration[nameof(intermediaryOptions.CallbackRoute)] ?? "callback";
                intermediaryOptions.WaitDurationInDays = double.TryParse(Configuration[nameof(intermediaryOptions.WaitDurationInDays)], out double waitDurationInDays) ? waitDurationInDays : 10;
                intermediaryOptions.CallbackResourceAuthentication = new BasicHttpAuthenticationResponse(new BasicHttpAuthentication("client-identifier", "client-secret"), "Intermediary API Callback Resource");
                intermediaryOptions.StartProcessingResourceAuthentication = new BasicHttpAuthenticationRequest(new BasicHttpAuthentication("client-identifier", "client-secret"));
            });

            services.AddControllers();

            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(0, 1);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public static void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
