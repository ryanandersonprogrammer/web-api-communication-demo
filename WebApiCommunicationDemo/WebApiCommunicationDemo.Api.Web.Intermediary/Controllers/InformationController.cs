﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Intermediary Information API
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Common.Attributes;
using WebApiCommunicationDemo.Api.Web.Service;
using WebApiCommunicationDemo.Api.Web.Service.Models;

namespace WebApiCommunicationDemo.Api.Web.Intermediary.Controllers
{
    /// <summary>
    /// This web API contains an endpoint for retrieving records that contain details about information that was or is being processed by a third-party web API.
    /// </summary>
    [ApiController]
    [ApiVersion("0.1")]
    [Route("intermediary/v{v:apiVersion}/[controller]")]
    [WebApiExceptionFilter]
    public class InformationController : ControllerBase
    {
        private readonly IIntermediaryService _intermediaryService;

        /// <summary>
        /// This constructor can be used to initialize a <see cref="InformationController"/>.
        /// </summary>
        /// <param name="intermediaryService">This is a service that handles the logic that is associated with requests between an intermediary and a third party.</param>
        public InformationController(IIntermediaryService intermediaryService)
        {
            _intermediaryService = intermediaryService ?? throw new ArgumentNullException(nameof(intermediaryService));
        }

        /// <summary>
        /// This endpoint will return the state of a callback request if the given identifier matches that of a record in the store.
        /// </summary>
        /// <remarks>
        /// This endpoint will return a record that contains a status, time stamps, and other details about a document that is being processed.
        /// </remarks>
        /// <param name="id">This is the identifier of a callback state.</param>
        /// <returns>A status response is returned from this endpoint if a request was successful.</returns>
        [HttpGet("status/{id}")]
        [ValidateModel]
        public async Task<ActionResult<IntermediaryCallbackState>> GetStatusAsync([FromRoute(Name = "id")][Required][StringLength(36, MinimumLength = 36)][RegularExpression("[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}")] string id)
        {
            return await _intermediaryService.RetrieveStatusResponseAsync(id).ConfigureAwait(false);
        }
    }
}
