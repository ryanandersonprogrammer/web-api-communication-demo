﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 Intermediary Communication API
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using WebApiCommunicationDemo.Api.Web.Common.Attributes;
using WebApiCommunicationDemo.Api.Web.Security.Attributes;
using WebApiCommunicationDemo.Api.Web.Service;
using WebApiCommunicationDemo.Api.Web.Service.Models;
using WebApiCommunicationDemo.Extensions.Http;

namespace WebApiCommunicationDemo.Api.Web.Intermediary.Controllers
{
    /// <summary>
    /// This web API contains endpoints for communicating with a third-party web API to process information.
    /// </summary>
    [ApiController]
    [ApiVersion("0.1")]
    [Route("intermediary/v{v:apiVersion}/[controller]")]
    [WebApiExceptionFilter]
    public class CommunicationController : ControllerBase
    {
        private readonly IIntermediaryService _intermediaryService;

        /// <summary>
        /// This constructor can be used to initialize a <see cref="InformationController"/>.
        /// </summary>
        /// <param name="intermediaryService">This is a service that handles the logic that is associated with requests between an intermediary and a third party.</param>
        public CommunicationController(IIntermediaryService intermediaryService)
        {
            _intermediaryService = intermediaryService ?? throw new ArgumentNullException(nameof(intermediaryService));
        }

        /// <summary>
        /// This endpoint can be used to send a request to a third-party web API to start the processing of information.
        /// </summary>
        /// <remarks>
        /// This endpoint will communicate with a third-party web API to start a process and create a record to keep track of statuses.
        /// </remarks>
        /// <param name="intermediaryCallbackState">This is a payload that contains a body of text and a callback URI.</param>
        /// <returns>This endpoint will return a status from the third-party web API if the request succeeds.</returns>
        [HttpPost("request")]
        [ValidateModel]
        public async Task<ActionResult<string>> PostRequestAsync([FromBody][Required] IntermediaryCallbackState intermediaryCallbackState)
        {
            return await _intermediaryService.SendInitialRequestAsync(intermediaryCallbackState).ConfigureAwait(false);
        }

        /// <summary>
        /// This endpoint can be used to accept from a third-party web API a callback request that indicates that a process has started.
        /// </summary>
        /// <remarks>
        /// This endpoint will expect the following word to be passed as an argument: started.
        /// </remarks>
        /// <returns>This endpoint will return a status of 204 if the request succeeds.</returns>
        [HttpPost("callback")]
        [AuthorizeWithBasicHttpAuthentication("client-identifier", "client-secret", "Intermediary API Callback Resource")]
        public async Task<IActionResult> PostToCallbackAsync()
        {
            var requestBodyText = await Request.ReadRawContentAsync().ConfigureAwait(false);

            await _intermediaryService.ReceiveInitialRequestFromThirdPartyServiceAsync(requestBodyText).ConfigureAwait(false);

            return NoContent();
        }

        /// <summary>
        /// This endpoint can be used to accept a callback request from a third-party web API to make status updates after a process has started.
        /// </summary>
        /// <remarks>
        /// This endpoint will expect strings containing the following words to be passed as arguments: processed, error, or completed.
        /// </remarks>
        /// <param name="intermediaryCallbackState">This payload contains a status and detail.</param>
        /// <returns>This endpoint will return a status of 204 if the request succeeds.</returns>
        [HttpPut("callback")]
        [AuthorizeWithBasicHttpAuthentication("client-identifier", "client-secret", "Intermediary API Callback Resource")]
        [ValidateModel]
        public async Task<IActionResult> PutToCallbackAsync([FromBody][Required] IntermediaryCallbackState intermediaryCallbackState)
        {
            await _intermediaryService.UpdateStateAsync(intermediaryCallbackState).ConfigureAwait(false);

            return NoContent();
        }
    }
}
