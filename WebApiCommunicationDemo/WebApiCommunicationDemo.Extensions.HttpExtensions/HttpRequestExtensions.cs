﻿/* Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Web API Communication Demo v0.1.0 HTTP Request Extensions
 * 
 * by Ryan E. Anderson
 * 
 * Copyright (c) 2020 Ryan E. Anderson
 */
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace WebApiCommunicationDemo.Extensions.Http
{
    /// <summary>
    /// This class contains extensions for instances of <see cref="HttpRequest"/>.
    /// </summary>
    public static class HttpRequestExtensions
    {
        /// <summary>
        /// This extension method can be used to read a body from an HTTP request as raw text.
        /// </summary>
        /// <param name="request">This is an HTTP request.</param>
        /// <param name="encoding">This is the encoding of the text.</param>
        /// <returns>This method returns the raw content from the body of an HTTP request.</returns>
        public async static Task<string> ReadRawContentAsync(this HttpRequest request, Encoding encoding = null)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (encoding == null)
                encoding = Encoding.UTF8;

            using StreamReader reader = new StreamReader(request.Body, encoding);

            return await reader.ReadToEndAsync().ConfigureAwait(false);
        }
    }
}
